<?php
putenv('ADA_OR_WISP=ADA');
putenv('MULTIPROVIDER=0');
// putenv('DEFAULT_PROVIDER_POINTER=default');
// putenv('DEFAULT_PROVIDER_DB=ada_learning_provider0');
putenv('PORTAL_NAME=ADA - Scuola');
putenv('MYSQL_HOST=localhost');
putenv('MYSQL_USER=');
putenv('MYSQL_PASSWORD=');
// below line is the common database name
putenv('MYSQL_DATABASE=ada_common');
// use first provider as http root, should be ok
putenv('HTTP_ROOT_DIR=https://primaria.lynxlab.com');
putenv('ADA_ADMIN_MAIL_ADDRESS=info@lynxlab.com');
putenv('ADA_NOREPLY_MAIL_ADDRESS=noreply@lynxlab.com');
