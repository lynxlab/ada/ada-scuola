ADA Social Learning
===================

Presentazione
-------------

ADA è una piattaforma di e-learning che Lynx ha progettato e sviluppato
a partire dal 2000. Si tratta di una piattaforma costruita intorno ad
un'idea di didattica di gruppo, in cui gli studenti e il docente
lavorano insieme per costruire conoscenza. Non simula una lezione in
presenza, ma permette di progettare delle attività didattiche che
richiedono un supporto digitale, in presenza o a distanza. E\' pensata
per un utilizzo da parte dei singoli studenti all\'interno di un
gruppo-classe ed è basata sulla rielaborazione continua di contenuti
didattici inseriti dal docente o prodotti dagli studenti stessi, da soli
o in gruppo.

Dopo le versioni specializzate per l'ECM (Educazione Continua in
Medicina) e per la sicurezza sul lavoro (ADA Safety), siamo lieti di
annunciare la nascita di ADA Social Learning, dedicata in particolare
alle scuole, ai bambini ed ai ragazzi.

ADA è opensource, il che significa non solo che chiunque sia in grado
può scaricarla e installarla, ma anche che può controllare il codice
sorgente per assicurarsi che non ci siano comportamenti illeciti o furti
di dati personali.

Mentre però altre piattaforme opensource hanno centinaia di moduli
sviluppati da soggetti esterni, di cui ovviamente è difficile verificare
la qualità, ADA è sviluppata interamente da Lynx con un'architettura
unitaria. Significa che possiamo garantire la qualità di tutto il codice
sorgente, che continuiamo a migliorarlo sulla base delle esperienze
d\'uso nostre e dei nostri clienti.

Di seguito descriviamo le caratteristiche che rendono ADA Social
Learning adatta in un contesto di bambini e ragazzi.

## 2. Caratteristiche speciali
---------------------------

### 2.1 Privacy

Tutte le versioni di ADA hanno una gestione della privacy integrata da
maggio 2018, data di entrata in vigore del GDPR. Anche in ADA SL vengono
gestite le richieste di accesso, modifica, cancellazione, esportazione
secondo quanto previsto dal GDPR.

ADA SL è zero-risk, ovvero i dati personali degli studenti registrati
possono essere ridotti al solo username: non sono richiesti nome e
cognome reali e nemmeno un'email. In questo modo, non vengono mai
registrati dati personali e quindi i rischi relativi ad un Data Breach
(un furto di dati personali, perseguibile ai sensi del GDPR) sono
annullati.

ADA SL, inoltre, consente di richiedere ai genitori, o a chi è
responsabile del minore, di fornire l'accettazione delle policy di
riservatezza adottate dalla scuola.

L'accesso agli strumenti di comunicazioni integrati, come ad esempio la
Videoconferenza, avviene solo attraverso la piattaforma, ed è perciò
protetto da accessi esterni.

### 2.2 Interfaccia specializzata per i diversi livelli

A differenza delle piattaforma generiche che si trovano su Cloud, ADA è
progettata da un team che ha oltre vent'anni di esperienza nel software
educativo per i bambini e i ragazzi. Questo significa che abbiamo ben
chiaro che l'interfaccia per bambini di sei anni e quella per ragazzi di
diciassette non può essere identica. Una sola installazione di ADA
permette di gestire accessi diversi, completamente personalizzati e
personalizzabili, con colori, font, menù e funzionalità adatte ai tre
livelli della scuola dell'obbligo (primaria, secondaria inferiore e
secondaria superiore).

E' possibile, ad esempio per un Istituto Comprensivo, fornire tre
accessi separati ai suoi allievi e ai suoi docenti, mantenendo un'unica
piattaforma e un solo contratto di servizio.

### 2.3 Interazione scuola-famiglia

Oltre ai ruoli tipici di ADA (studente, docente, coordinatore,
amministratore) ADA SL offre in più il ruolo di **genitore**. Si tratta
di un utente che al momento della registrazione deve essere associato ad
uno o più studenti, e che da quel momento può vedere i report di quegli
studenti e comunicare con i loro docenti, usando tutti gli strumenti
messi a disposizione dal ADA. _(Nota: questa funzionalità è attualmente in
sviluppo)_


## 2.4 Contenuti
-------------

### 2.4.1 Creazione di contenuti

In ADA è possibile la creazione di contenuti direttamente nella
piattaforma con un editor integrato con cui si possono inserire testi,
immagini, ma anche audio e video esterni.

Una caratteristica unica di ADA è la possibilità di creare delle mappe
concettuali a partire dai contenuti del corso, senza ricorrere a
strumenti aggiuntivi. ADA è dotata di un sofisticato modulo di creazione
di test che consente di generare scelte multiple, risposte aperte ma
anche cloze, utilizzando testo, immagini, audio e video.

### 2.4.2 Riuso di contenuti

ADA è è centrata sul concetto di riuso. I contenuti e le attività
vengono inseriti in *corsi* che possono corrispondere alla materie
scolastiche tradizionali (Italiano per la prima classe, Matematica per
la seconda classe, etc\...) ma anche ad aree miste e interdisciplinari.
Ad ogni corso si possono iscrivere classi di studenti, creando così
diverse *edizioni* di quel corso (ad esempio, una per ogni sezione:
Matematica IA, Matematica IB, etc).\
Questo significa che i contenuti di un corso, stabiliti dai singoli
docenti o da un coordinamento, possono essere riusati immediatamente
senza doverli ricreare ogni volta da capo. Ogni edizione ha naturalmente
uno o più docenti di riferimento, un proprio calendario, e può essere
arricchita con commenti e file indipendentemente da tutte le altre.

Oltre a creare direttamente contenuti con l'Editor integrato, ADA
permette la creazione di lezioni a partire da risorse didattiche
esistenti (slide, video, link) scelte tra quelle della Repository
comune.

Allo stesso modo, si possono inserire i quiz scelti tra quelli della
Repository comune. Questi strumenti sono particolarmente utile per la
scuola perché permettono la condivisione e il riuso di risorse tra
classi e tra docenti.

## 2.5 Gaming e badge
------------------

Il gaming è uno degli strumenti e delle modalità di interazione più
funzionali per i bambini. Non si tratta di inserire videogiochi tra i
contenuti, ma di strutturare l'ambiente di apprendimento come una caccia
al tesoro, come un percorso in cui al bambino/ragazzo vengano presentate
delle scelte. Utilizzi tipici sono quelli della simulazione di un
ambiente fisico, ma si possono utilizzare anche per l'apprendimento
della Storia ("cosa sarebbe successo se\..."), dell'Educazione Civica,
delle Lingue, eccetera.

Mentre nel caso di altre piattaforme la possibilità di gestire storie a
bivi è legata all'installazione di un apposito plugin, nel caso di ADA
non sono necessari plugin, ma è la struttura stessa dei contenuti ad
essere reticolare e quindi a rendere immediata la realizzazione di un
percorso a bivi.

La navigazione in forma di percorso di gioco è immediata: vengono
disattivati tutti gli strumenti di navigazione (indice, mappa, ricerca)
che toglierebbero attrattiva all'attività giocosa.

Lo stesso va detto per i Badge intermedi e finali che si guadagnano
durante il percorso. I badge sono agganciati a delle condizioni di
completamento, come la percentuale di nodi visitata, il tempo trascorso
oppure, nel caso di un percorso di gaming, al raggiungimento di
determinati livelli di competenza.

## 2.6 Videoconferenze e comunicazioni sincrone/asincrone
------------------------------------------------------

### 2.6.1 Videoconferenze

ADA non contiene uno specifico sistema di videoconferenza, ma può
integrarne diversi, con preferenza per quelli Open Source come Jitsi e
BigBlueButton. E\' possibile utilizzare un sistema già presente nella
scuola, installarne uno da zero, oppure sceglierne uno offerto dagli
operatori del mercato.

Ogni classe ha a disposizione una stanza di videoconferenza, che può
essere aperta solo dal docente; è anche possibile attivare una stanza
privata per il dialogo tra docente e studente.

Anche nel caso di sistemi non opensource, abbiamo curato l\'integrazione
in modo che per lo studente e il docente l\'accesso ad un sistema
diverso sia trasparente, senza necessità di aprire finestre separate o
addirittura applicazioni diverse. In questo modo siamo anche più sicuri
che l\'accesso alla stanza di videoconferenza di classe possa avvenire
solo dopo aver fatto il login dentro ADA.

### 2.6.2 Comunicazioni

La videoconferenza, come è ormai chiaro a tutti, consuma molta banda, a volte anche inutilmente. Oltre ad essere un'invasione della privacy, spesso mette in difficoltà chi si connette da casa (le famiglie o i docenti), ma anche la scuola stessa in caso di uso
massiccio da parte di tutte le classi contemporaneamente. Per questo, raccomandiamo sempre di _non far accendere tutte le webcam_ a tutti gli studenti contemporaneamente e di usare la videoconferenza non come sostituto generico della lezione uno-a-molti ma per uno scopo preciso e con una durata limitata (ad esempio: per organizzare il lavoro in piccoli gruppi, per risolvere problemi che richiedono di esibire oggetti o azioni, per intervistare esperti esterni).

In ogni caso, oltre alla videoconferenza ADA consente al docente di inviare messaggi a tutta la classe o al singolo studente in altri modi meno pesanti: con un messaggio interno, con una chat testuale, con un appuntamento in agenda oppure attraverso i commenti ai contenuti didattici.
