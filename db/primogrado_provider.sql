-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: mariadb    Database: primogrado_provider
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB-1:10.4.13+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ADA_moreUserFields`
--

DROP TABLE IF EXISTS `ADA_moreUserFields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADA_moreUserFields` (
  `idMoreUserFields` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `studente_id_utente_studente` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMoreUserFields`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADA_moreUserFields`
--

LOCK TABLES `ADA_moreUserFields` WRITE;
/*!40000 ALTER TABLE `ADA_moreUserFields` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADA_moreUserFields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ADA_someMoreUserFields`
--

DROP TABLE IF EXISTS `ADA_someMoreUserFields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADA_someMoreUserFields` (
  `idSomeMoreUserFields` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `foreign_key` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSomeMoreUserFields`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADA_someMoreUserFields`
--

LOCK TABLES `ADA_someMoreUserFields` WRITE;
/*!40000 ALTER TABLE `ADA_someMoreUserFields` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADA_someMoreUserFields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amministratore_corsi`
--

DROP TABLE IF EXISTS `amministratore_corsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amministratore_corsi` (
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente_amministratore` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_corso`,`id_utente_amministratore`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amministratore_corsi`
--

LOCK TABLES `amministratore_corsi` WRITE;
/*!40000 ALTER TABLE `amministratore_corsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `amministratore_corsi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amministratore_sistema`
--

DROP TABLE IF EXISTS `amministratore_sistema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amministratore_sistema` (
  `id_utente_amministratore_sist` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente_amministratore_sist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amministratore_sistema`
--

LOCK TABLES `amministratore_sistema` WRITE;
/*!40000 ALTER TABLE `amministratore_sistema` DISABLE KEYS */;
INSERT INTO `amministratore_sistema` VALUES (1);
/*!40000 ALTER TABLE `amministratore_sistema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autore`
--

DROP TABLE IF EXISTS `autore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autore` (
  `id_utente_autore` int(10) unsigned NOT NULL DEFAULT 0,
  `profilo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tariffa` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente_autore`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autore`
--

LOCK TABLES `autore` WRITE;
/*!40000 ALTER TABLE `autore` DISABLE KEYS */;
/*!40000 ALTER TABLE `autore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id_banner` int(10) NOT NULL AUTO_INCREMENT,
  `address` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_client` int(10) NOT NULL DEFAULT 0,
  `id_course` int(10) NOT NULL DEFAULT 0,
  `module` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `impressions` int(11) NOT NULL DEFAULT 0,
  `a_impressions` int(11) NOT NULL DEFAULT 0,
  `date_from` int(11) DEFAULT NULL,
  `date_to` int(11) DEFAULT NULL,
  KEY `id_banner` (`id_banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookmark`
--

DROP TABLE IF EXISTS `bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark` (
  `id_bookmark` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_utente_studente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `data` int(11) NOT NULL DEFAULT 0,
  `descrizione` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_bookmark`),
  KEY `bookmark_date` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookmark`
--

LOCK TABLES `bookmark` WRITE;
/*!40000 ALTER TABLE `bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chatroom`
--

DROP TABLE IF EXISTS `chatroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom` (
  `id_chatroom` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo_chat` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titolo_chat` text COLLATE utf8_unicode_ci NOT NULL,
  `argomento_chat` text COLLATE utf8_unicode_ci NOT NULL,
  `id_proprietario_chat` int(10) unsigned NOT NULL DEFAULT 0,
  `tempo_avvio` int(11) NOT NULL DEFAULT 0,
  `tempo_fine` int(11) NOT NULL DEFAULT 0,
  `msg_benvenuto` text COLLATE utf8_unicode_ci NOT NULL,
  `max_utenti` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_chatroom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chatroom`
--

LOCK TABLES `chatroom` WRITE;
/*!40000 ALTER TABLE `chatroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `chatroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clienti`
--

DROP TABLE IF EXISTS `clienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienti` (
  `id_client` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `clienti_id` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienti`
--

LOCK TABLES `clienti` WRITE;
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destinatari_messaggi`
--

DROP TABLE IF EXISTS `destinatari_messaggi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinatari_messaggi` (
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_messaggio` int(10) unsigned NOT NULL DEFAULT 0,
  `read_timestamp` int(11) NOT NULL DEFAULT 0,
  `deleted` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id_utente`,`id_messaggio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destinatari_messaggi`
--

LOCK TABLES `destinatari_messaggi` WRITE;
/*!40000 ALTER TABLE `destinatari_messaggi` DISABLE KEYS */;
/*!40000 ALTER TABLE `destinatari_messaggi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extended_node`
--

DROP TABLE IF EXISTS `extended_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extended_node` (
  `id_node` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `hyphenation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grammar` text COLLATE utf8_unicode_ci NOT NULL,
  `semantic` text COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `examples` text COLLATE utf8_unicode_ci NOT NULL,
  `language` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_node`
--

LOCK TABLES `extended_node` WRITE;
/*!40000 ALTER TABLE `extended_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `extended_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_esercizi`
--

DROP TABLE IF EXISTS `history_esercizi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_esercizi` (
  `ID_HISTORY_EX` int(10) NOT NULL AUTO_INCREMENT,
  `ID_UTENTE_STUDENTE` int(10) NOT NULL DEFAULT 0,
  `ID_NODO` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ID_ISTANZA_CORSO` int(10) NOT NULL DEFAULT 0,
  `DATA_VISITA` int(11) NOT NULL DEFAULT 0,
  `DATA_USCITA` int(11) DEFAULT NULL,
  `RISPOSTA_LIBERA` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENTO` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `PUNTEGGIO` smallint(4) DEFAULT NULL,
  `CORREZIONE_RISPOSTA_LIBERA` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `RIPETIBILE` smallint(1) NOT NULL DEFAULT 0,
  `ALLEGATO` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_HISTORY_EX`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_esercizi`
--

LOCK TABLES `history_esercizi` WRITE;
/*!40000 ALTER TABLE `history_esercizi` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_esercizi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_nodi`
--

DROP TABLE IF EXISTS `history_nodi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_nodi` (
  `id_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente_studente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `data_visita` int(11) NOT NULL DEFAULT 0,
  `data_uscita` int(11) NOT NULL DEFAULT 0,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `remote_address` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installation_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_from` smallint(5) unsigned DEFAULT 0,
  PRIMARY KEY (`id_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_nodi`
--

LOCK TABLES `history_nodi` WRITE;
/*!40000 ALTER TABLE `history_nodi` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_nodi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iscrizioni`
--

DROP TABLE IF EXISTS `iscrizioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iscrizioni` (
  `id_utente_studente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `livello` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `codice` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_iscrizione` int(11) DEFAULT NULL,
  `laststatusupdate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_utente_studente`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iscrizioni`
--

LOCK TABLES `iscrizioni` WRITE;
/*!40000 ALTER TABLE `iscrizioni` DISABLE KEYS */;
/*!40000 ALTER TABLE `iscrizioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `istanza_corso`
--

DROP TABLE IF EXISTS `istanza_corso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `istanza_corso` (
  `id_istanza_corso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `data_inizio` int(11) NOT NULL DEFAULT 0,
  `durata` int(10) unsigned DEFAULT NULL,
  `data_inizio_previsto` int(11) NOT NULL DEFAULT 0,
  `id_layout` int(10) unsigned NOT NULL DEFAULT 0,
  `data_fine` int(11) NOT NULL DEFAULT 0,
  `status` int(10) unsigned NOT NULL DEFAULT 0,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(7,2) NOT NULL,
  `self_instruction` tinyint(1) NOT NULL,
  `self_registration` tinyint(1) NOT NULL,
  `start_level_student` int(2) NOT NULL,
  `duration_subscription` int(3) NOT NULL,
  `open_subscription` tinyint(1) NOT NULL,
  `duration_hours` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo_servizio` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `istanza_corso`
--

LOCK TABLES `istanza_corso` WRITE;
/*!40000 ALTER TABLE `istanza_corso` DISABLE KEYS */;
/*!40000 ALTER TABLE `istanza_corso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `id_link` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_nodo_to` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_posizione` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `data_creazione` int(11) DEFAULT NULL,
  `stile` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `significato` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `azione` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_link`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link`
--

LOCK TABLES `link` WRITE;
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
/*!40000 ALTER TABLE `link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_classi`
--

DROP TABLE IF EXISTS `log_classi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_classi` (
  `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `visite` int(10) unsigned NOT NULL DEFAULT 0,
  `punti` int(10) unsigned NOT NULL DEFAULT 0,
  `esercizi` int(10) unsigned NOT NULL DEFAULT 0,
  `msg_out` int(10) unsigned NOT NULL DEFAULT 0,
  `msg_in` int(10) unsigned NOT NULL DEFAULT 0,
  `notes_in` int(10) unsigned NOT NULL DEFAULT 0,
  `notes_out` int(10) unsigned NOT NULL DEFAULT 0,
  `chat` int(10) unsigned NOT NULL DEFAULT 0,
  `bookmarks` int(10) unsigned NOT NULL DEFAULT 0,
  `indice_att` int(10) unsigned NOT NULL DEFAULT 0,
  `level` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `last_access` int(11) NOT NULL DEFAULT 0,
  `exercises_test` int(10) unsigned NOT NULL DEFAULT 0,
  `score_test` int(10) unsigned NOT NULL DEFAULT 0,
  `exercises_survey` int(10) unsigned NOT NULL DEFAULT 0,
  `score_survey` int(10) unsigned NOT NULL DEFAULT 0,
  `subscription_status` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_classi`
--

LOCK TABLES `log_classi` WRITE;
/*!40000 ALTER TABLE `log_classi` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_classi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggi`
--

DROP TABLE IF EXISTS `messaggi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggi` (
  `id_messaggio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_group` int(10) unsigned NOT NULL DEFAULT 0,
  `data_ora` int(11) NOT NULL DEFAULT 0,
  `tipo` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_mittente` int(10) unsigned DEFAULT NULL,
  `priorita` tinyint(3) unsigned DEFAULT NULL,
  `testo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `flags` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_messaggio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggi`
--

LOCK TABLES `messaggi` WRITE;
/*!40000 ALTER TABLE `messaggi` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modello_corso`
--

DROP TABLE IF EXISTS `modello_corso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modello_corso` (
  `id_corso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente_autore` int(10) unsigned NOT NULL,
  `id_layout` int(10) unsigned DEFAULT 0,
  `nome` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titolo` text COLLATE utf8_unicode_ci NOT NULL,
  `data_creazione` int(11) DEFAULT NULL,
  `data_pubblicazione` int(11) DEFAULT NULL,
  `descrizione` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nodo_iniziale` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_nodo_toc` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `media_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `static_mode` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `id_lingua` tinyint(3) unsigned NOT NULL,
  `crediti` tinyint(3) NOT NULL DEFAULT 1,
  `duration_hours` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo_servizio` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_corso`),
  UNIQUE KEY `modello_corso_nome` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modello_corso`
--

LOCK TABLES `modello_corso` WRITE;
/*!40000 ALTER TABLE `modello_corso` DISABLE KEYS */;
/*!40000 ALTER TABLE `modello_corso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_badges_badges`
--

DROP TABLE IF EXISTS `module_badges_badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_badges_badges` (
  `uuid_bin` binary(16) NOT NULL,
  `name` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`uuid_bin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_badges_badges`
--

LOCK TABLES `module_badges_badges` WRITE;
/*!40000 ALTER TABLE `module_badges_badges` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_badges_badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_badges_course_badges`
--

DROP TABLE IF EXISTS `module_badges_course_badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_badges_course_badges` (
  `badge_uuid_bin` binary(16) NOT NULL,
  `id_corso` int(10) unsigned DEFAULT NULL,
  `id_conditionset` int(10) unsigned NOT NULL,
  UNIQUE KEY `course_badges_idx` (`badge_uuid_bin`,`id_corso`,`id_conditionset`) USING BTREE,
  KEY `fk_badge_uuid` (`badge_uuid_bin`),
  KEY `fk_id_conditionset` (`id_conditionset`),
  CONSTRAINT `fk_badge_uuid` FOREIGN KEY (`badge_uuid_bin`) REFERENCES `module_badges_badges` (`uuid_bin`) ON DELETE CASCADE,
  CONSTRAINT `fk_id_conditionset` FOREIGN KEY (`id_conditionset`) REFERENCES `module_complete_conditionset` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_badges_course_badges`
--

LOCK TABLES `module_badges_course_badges` WRITE;
/*!40000 ALTER TABLE `module_badges_course_badges` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_badges_course_badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_badges_rewarded_badges`
--

DROP TABLE IF EXISTS `module_badges_rewarded_badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_badges_rewarded_badges` (
  `uuid_bin` binary(16) NOT NULL,
  `badge_uuid_bin` binary(16) NOT NULL,
  `issuedOn` int(10) unsigned NOT NULL,
  `approved` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `notified` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL,
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  PRIMARY KEY (`uuid_bin`),
  UNIQUE KEY `unique_badge_student_course` (`badge_uuid_bin`,`id_utente`,`id_corso`,`id_istanza_corso`),
  KEY `fk_rewarded_badge_uuid` (`badge_uuid_bin`),
  KEY `fk_rewarded_badge_id_corso` (`id_corso`),
  KEY `fk_rewarded_badge_id_istanza_corso` (`id_istanza_corso`),
  KEY `fk_rewarded_badge_id_utente` (`id_utente`),
  CONSTRAINT `fk_rewarded_badge_id_corso` FOREIGN KEY (`id_corso`) REFERENCES `modello_corso` (`id_corso`) ON DELETE CASCADE,
  CONSTRAINT `fk_rewarded_badge_id_istanza_corso` FOREIGN KEY (`id_istanza_corso`) REFERENCES `istanza_corso` (`id_istanza_corso`) ON DELETE CASCADE,
  CONSTRAINT `fk_rewarded_badge_id_utente` FOREIGN KEY (`id_utente`) REFERENCES `utente` (`id_utente`) ON DELETE CASCADE,
  CONSTRAINT `fk_rewarded_badge_uuid` FOREIGN KEY (`badge_uuid_bin`) REFERENCES `module_badges_badges` (`uuid_bin`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_badges_rewarded_badges`
--

LOCK TABLES `module_badges_rewarded_badges` WRITE;
/*!40000 ALTER TABLE `module_badges_rewarded_badges` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_badges_rewarded_badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classagenda_calendars`
--

DROP TABLE IF EXISTS `module_classagenda_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classagenda_calendars` (
  `module_classagenda_calendars_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start` int(11) unsigned NOT NULL,
  `end` int(11) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `id_classroom` int(10) unsigned DEFAULT NULL,
  `id_utente_tutor` int(10) unsigned NOT NULL,
  PRIMARY KEY (`module_classagenda_calendars_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classagenda_calendars`
--

LOCK TABLES `module_classagenda_calendars` WRITE;
/*!40000 ALTER TABLE `module_classagenda_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classagenda_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classagenda_reminder_history`
--

DROP TABLE IF EXISTS `module_classagenda_reminder_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classagenda_reminder_history` (
  `module_classagenda_reminder_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_classagenda_calendars_id` int(10) unsigned NOT NULL,
  `html` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` int(11) NOT NULL,
  PRIMARY KEY (`module_classagenda_reminder_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classagenda_reminder_history`
--

LOCK TABLES `module_classagenda_reminder_history` WRITE;
/*!40000 ALTER TABLE `module_classagenda_reminder_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classagenda_reminder_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classagenda_rollcall`
--

DROP TABLE IF EXISTS `module_classagenda_rollcall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classagenda_rollcall` (
  `module_classagenda_rollcall_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente_studente` int(10) NOT NULL,
  `module_classagenda_calendars_id` int(10) NOT NULL,
  `entertime` int(11) unsigned NOT NULL,
  `exittime` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`module_classagenda_rollcall_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classagenda_rollcall`
--

LOCK TABLES `module_classagenda_rollcall` WRITE;
/*!40000 ALTER TABLE `module_classagenda_rollcall` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classagenda_rollcall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_budget_instance`
--

DROP TABLE IF EXISTS `module_classbudget_budget_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_budget_instance` (
  `budget_instance_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `budget` decimal(8,2) unsigned DEFAULT NULL,
  `references` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`budget_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_budget_instance`
--

LOCK TABLES `module_classbudget_budget_instance` WRITE;
/*!40000 ALTER TABLE `module_classbudget_budget_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_budget_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_cost_classroom`
--

DROP TABLE IF EXISTS `module_classbudget_cost_classroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_cost_classroom` (
  `cost_classroom_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_classroom` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `hourly_rate` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`cost_classroom_id`),
  UNIQUE KEY `id_classroom_instance` (`id_classroom`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_cost_classroom`
--

LOCK TABLES `module_classbudget_cost_classroom` WRITE;
/*!40000 ALTER TABLE `module_classbudget_cost_classroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_cost_classroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_cost_item`
--

DROP TABLE IF EXISTS `module_classbudget_cost_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_cost_item` (
  `cost_item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `applied_to` int(2) DEFAULT NULL,
  PRIMARY KEY (`cost_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_cost_item`
--

LOCK TABLES `module_classbudget_cost_item` WRITE;
/*!40000 ALTER TABLE `module_classbudget_cost_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_cost_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_cost_tutor`
--

DROP TABLE IF EXISTS `module_classbudget_cost_tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_cost_tutor` (
  `cost_tutor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tutor` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `hourly_rate` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`cost_tutor_id`),
  UNIQUE KEY `id_classroom_instance` (`id_tutor`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_cost_tutor`
--

LOCK TABLES `module_classbudget_cost_tutor` WRITE;
/*!40000 ALTER TABLE `module_classbudget_cost_tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_cost_tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classroom_classrooms`
--

DROP TABLE IF EXISTS `module_classroom_classrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classroom_classrooms` (
  `id_classroom` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_venue` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seats` int(4) unsigned DEFAULT NULL,
  `computers` tinyint(4) unsigned DEFAULT NULL,
  `internet` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `wifi` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `projector` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `mobility_impaired` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `hourly_rate` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id_classroom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classroom_classrooms`
--

LOCK TABLES `module_classroom_classrooms` WRITE;
/*!40000 ALTER TABLE `module_classroom_classrooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classroom_classrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classroom_venues`
--

DROP TABLE IF EXISTS `module_classroom_venues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classroom_venues` (
  `id_venue` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressline1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addressline2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_venue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classroom_venues`
--

LOCK TABLES `module_classroom_venues` WRITE;
/*!40000 ALTER TABLE `module_classroom_venues` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classroom_venues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_complete_conditionset`
--

DROP TABLE IF EXISTS `module_complete_conditionset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_complete_conditionset` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descrizione` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_complete_conditionset`
--

LOCK TABLES `module_complete_conditionset` WRITE;
/*!40000 ALTER TABLE `module_complete_conditionset` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_complete_conditionset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_complete_conditionset_course`
--

DROP TABLE IF EXISTS `module_complete_conditionset_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_complete_conditionset_course` (
  `id_conditionset` int(10) unsigned NOT NULL COMMENT 'id of the completeset rule',
  `id_course` int(10) unsigned NOT NULL COMMENT 'id of the course linked to the completeset rule',
  PRIMARY KEY (`id_conditionset`,`id_course`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_complete_conditionset_course`
--

LOCK TABLES `module_complete_conditionset_course` WRITE;
/*!40000 ALTER TABLE `module_complete_conditionset_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_complete_conditionset_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_complete_operations`
--

DROP TABLE IF EXISTS `module_complete_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_complete_operations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_conditionset` int(11) NOT NULL,
  `operator` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operand1` text COLLATE utf8_unicode_ci NOT NULL,
  `operand2` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` int(11) NOT NULL COMMENT 'this is called priority but it''s used to tell in which column of the UI is the conditionSet',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_complete_operations`
--

LOCK TABLES `module_complete_operations` WRITE;
/*!40000 ALTER TABLE `module_complete_operations` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_complete_operations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_forkedpaths_history`
--

DROP TABLE IF EXISTS `module_forkedpaths_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_forkedpaths_history` (
  `module_forkedpaths_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `courseInstanceId` int(10) unsigned NOT NULL,
  `nodeFrom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nodeTo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `userLevelFrom` tinyint(3) unsigned NOT NULL,
  `userLevelTo` tinyint(3) unsigned NOT NULL,
  `saveTS` int(10) unsigned NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`module_forkedpaths_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_forkedpaths_history`
--

LOCK TABLES `module_forkedpaths_history` WRITE;
/*!40000 ALTER TABLE `module_forkedpaths_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_forkedpaths_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_formmail_helptype`
--

DROP TABLE IF EXISTS `module_formmail_helptype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_formmail_helptype` (
  `module_formmail_helptype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recipient` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`module_formmail_helptype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_formmail_helptype`
--

LOCK TABLES `module_formmail_helptype` WRITE;
/*!40000 ALTER TABLE `module_formmail_helptype` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_formmail_helptype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_formmail_history`
--

DROP TABLE IF EXISTS `module_formmail_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_formmail_history` (
  `module_formmail_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL,
  `module_formmail_helptype_id` int(10) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msgbody` text COLLATE utf8_unicode_ci NOT NULL,
  `attachments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `selfSent` tinyint(1) unsigned NOT NULL,
  `sentOK` tinyint(1) unsigned NOT NULL,
  `sentTimestamp` int(10) NOT NULL,
  PRIMARY KEY (`module_formmail_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_formmail_history`
--

LOCK TABLES `module_formmail_history` WRITE;
/*!40000 ALTER TABLE `module_formmail_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_formmail_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_policy_content`
--

DROP TABLE IF EXISTS `module_gdpr_policy_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_policy_content` (
  `policy_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tester_pointer` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mandatory` tinyint(3) unsigned DEFAULT 0,
  `isPublished` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `lastEditTS` int(11) NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`policy_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_policy_content`
--

LOCK TABLES `module_gdpr_policy_content` WRITE;
/*!40000 ALTER TABLE `module_gdpr_policy_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_policy_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_policy_utente`
--

DROP TABLE IF EXISTS `module_gdpr_policy_utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_policy_utente` (
  `id_utente` int(10) unsigned NOT NULL,
  `id_policy` int(10) unsigned NOT NULL,
  `acceptedVersion` int(11) unsigned NOT NULL,
  `lastmodTS` int(11) unsigned NOT NULL,
  `isAccepted` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente`,`id_policy`,`acceptedVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_policy_utente`
--

LOCK TABLES `module_gdpr_policy_utente` WRITE;
/*!40000 ALTER TABLE `module_gdpr_policy_utente` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_policy_utente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_requestTypes`
--

DROP TABLE IF EXISTS `module_gdpr_requestTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_requestTypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_requestTypes`
--

LOCK TABLES `module_gdpr_requestTypes` WRITE;
/*!40000 ALTER TABLE `module_gdpr_requestTypes` DISABLE KEYS */;
INSERT INTO `module_gdpr_requestTypes` VALUES (1,4,'Cancellazione','{\"confirmhandle\":true}'),(2,1,'Accesso',NULL),(3,2,'Modifica',NULL),(4,3,'Limita','{\"confirmhandle\":true}'),(5,5,'Opposizione',NULL);
/*!40000 ALTER TABLE `module_gdpr_requestTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_requests`
--

DROP TABLE IF EXISTS `module_gdpr_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_requests` (
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `generatedBy` int(11) NOT NULL,
  `generatedTs` int(11) NOT NULL,
  `confirmedTs` int(11) DEFAULT NULL,
  `closedBy` int(11) DEFAULT NULL,
  `closedTs` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `selfOpened` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_requests`
--

LOCK TABLES `module_gdpr_requests` WRITE;
/*!40000 ALTER TABLE `module_gdpr_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_userTypes`
--

DROP TABLE IF EXISTS `module_gdpr_userTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_userTypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_userTypes`
--

LOCK TABLES `module_gdpr_userTypes` WRITE;
/*!40000 ALTER TABLE `module_gdpr_userTypes` DISABLE KEYS */;
INSERT INTO `module_gdpr_userTypes` VALUES (1,'Manager'),(2,'Nessuno');
/*!40000 ALTER TABLE `module_gdpr_userTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_users`
--

DROP TABLE IF EXISTS `module_gdpr_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_users` (
  `id_utente` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_utente`,`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_users`
--

LOCK TABLES `module_gdpr_users` WRITE;
/*!40000 ALTER TABLE `module_gdpr_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_history_login`
--

DROP TABLE IF EXISTS `module_login_history_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_history_login` (
  `id_utente` int(10) unsigned NOT NULL,
  `date` int(11) NOT NULL,
  `module_login_providers_id` int(5) unsigned NOT NULL,
  `successfulOptionsID` int(5) unsigned NOT NULL,
  PRIMARY KEY (`id_utente`,`date`,`module_login_providers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_history_login`
--

LOCK TABLES `module_login_history_login` WRITE;
/*!40000 ALTER TABLE `module_login_history_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_login_history_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_options`
--

DROP TABLE IF EXISTS `module_login_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_options` (
  `module_login_providers_options_id` int(5) unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`key`,`module_login_providers_options_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_options`
--

LOCK TABLES `module_login_options` WRITE;
/*!40000 ALTER TABLE `module_login_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_login_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_providers`
--

DROP TABLE IF EXISTS `module_login_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_providers` (
  `module_login_providers_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `className` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `buttonLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(4) unsigned NOT NULL,
  PRIMARY KEY (`module_login_providers_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_providers`
--

LOCK TABLES `module_login_providers` WRITE;
/*!40000 ALTER TABLE `module_login_providers` DISABLE KEYS */;
INSERT INTO `module_login_providers` VALUES (1,'adaLogin','Ada',1,'Accedi',1);
/*!40000 ALTER TABLE `module_login_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_providers_options`
--

DROP TABLE IF EXISTS `module_login_providers_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_providers_options` (
  `module_login_providers_options_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `module_login_providers_id` int(5) unsigned NOT NULL,
  `order` int(5) unsigned NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`module_login_providers_options_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_providers_options`
--

LOCK TABLES `module_login_providers_options` WRITE;
/*!40000 ALTER TABLE `module_login_providers_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_login_providers_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_newsletter_history`
--

DROP TABLE IF EXISTS `module_newsletter_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_newsletter_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_newsletter` int(10) unsigned DEFAULT NULL,
  `filter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datesent` int(11) NOT NULL,
  `recipientscount` int(6) unsigned NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_newsletter_history`
--

LOCK TABLES `module_newsletter_history` WRITE;
/*!40000 ALTER TABLE `module_newsletter_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_newsletter_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_newsletter_newsletters`
--

DROP TABLE IF EXISTS `module_newsletter_newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_newsletter_newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmltext` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `plaintext` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `draft` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_newsletter_newsletters`
--

LOCK TABLES `module_newsletter_newsletters` WRITE;
/*!40000 ALTER TABLE `module_newsletter_newsletters` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_newsletter_newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_studentsgroups_groups`
--

DROP TABLE IF EXISTS `module_studentsgroups_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_studentsgroups_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customField0` int(10) unsigned DEFAULT NULL COMMENT 'application managed',
  `customField1` int(10) unsigned DEFAULT NULL COMMENT 'application managed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_studentsgroups_groups`
--

LOCK TABLES `module_studentsgroups_groups` WRITE;
/*!40000 ALTER TABLE `module_studentsgroups_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_studentsgroups_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_studentsgroups_groups_utente`
--

DROP TABLE IF EXISTS `module_studentsgroups_groups_utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_studentsgroups_groups_utente` (
  `group_id` int(10) unsigned NOT NULL,
  `utente_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `group_utente_idx` (`group_id`,`utente_id`),
  KEY `fk_studentsgroups_utente` (`utente_id`),
  CONSTRAINT `fk_studentsgroups_groups` FOREIGN KEY (`group_id`) REFERENCES `module_studentsgroups_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_studentsgroups_utente` FOREIGN KEY (`utente_id`) REFERENCES `utente` (`id_utente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_studentsgroups_groups_utente`
--

LOCK TABLES `module_studentsgroups_groups_utente` WRITE;
/*!40000 ALTER TABLE `module_studentsgroups_groups_utente` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_studentsgroups_groups_utente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_course_survey`
--

DROP TABLE IF EXISTS `module_test_course_survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_course_survey` (
  `id_corso` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  `id_nodo` varchar(64) NOT NULL,
  PRIMARY KEY (`id_corso`,`id_test`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_course_survey`
--

LOCK TABLES `module_test_course_survey` WRITE;
/*!40000 ALTER TABLE `module_test_course_survey` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_course_survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_history_answer`
--

DROP TABLE IF EXISTS `module_test_history_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_history_answer` (
  `id_answer` int(10) NOT NULL AUTO_INCREMENT,
  `id_history_test` int(10) unsigned NOT NULL,
  `id_utente` int(10) NOT NULL DEFAULT 0,
  `id_topic` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id of relative topic test node',
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL COMMENT 'id of relative question test node',
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) DEFAULT NULL COMMENT 'id of relative course instance',
  `risposta` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'student''s answer (a serialized array)',
  `commento` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tutor''s comment',
  `punteggio` smallint(4) DEFAULT NULL,
  `correzione_risposta` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `allegato` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `data` int(10) NOT NULL,
  PRIMARY KEY (`id_answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_history_answer`
--

LOCK TABLES `module_test_history_answer` WRITE;
/*!40000 ALTER TABLE `module_test_history_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_history_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_history_test`
--

DROP TABLE IF EXISTS `module_test_history_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_history_test` (
  `id_history_test` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned DEFAULT NULL,
  `id_nodo` int(10) NOT NULL,
  `data_inizio` int(10) NOT NULL,
  `data_fine` int(10) NOT NULL,
  `punteggio_realizzato` int(10) unsigned NOT NULL DEFAULT 0,
  `ripetibile` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `punteggio_minimo_barriera` int(10) unsigned NOT NULL DEFAULT 0,
  `livello_raggiunto` int(10) unsigned DEFAULT NULL,
  `consegnato` tinyint(1) NOT NULL DEFAULT 0,
  `tempo_scaduto` tinyint(1) NOT NULL DEFAULT 0,
  `domande` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_history_test`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_history_test`
--

LOCK TABLES `module_test_history_test` WRITE;
/*!40000 ALTER TABLE `module_test_history_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_history_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_nodes`
--

DROP TABLE IF EXISTS `module_test_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_nodes` (
  `id_nodo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_posizione` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza` int(10) unsigned DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consegna` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `testo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` mediumint(8) unsigned DEFAULT NULL,
  `data_creazione` int(10) DEFAULT NULL,
  `ordine` int(10) DEFAULT NULL,
  `id_nodo_parent` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nodo_radice` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nodo_riferimento` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livello` int(10) unsigned DEFAULT 0,
  `versione` int(10) unsigned NOT NULL DEFAULT 0,
  `n_contatti` int(10) unsigned NOT NULL DEFAULT 0,
  `icona` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_didascalia` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_sfondo` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correttezza` tinyint(3) unsigned DEFAULT NULL,
  `copyright` tinyint(3) unsigned DEFAULT NULL,
  `didascalia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `durata` int(10) DEFAULT NULL,
  `titolo_dragdrop` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_nodes`
--

LOCK TABLES `module_test_nodes` WRITE;
/*!40000 ALTER TABLE `module_test_nodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nodo`
--

DROP TABLE IF EXISTS `nodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodo` (
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_posizione` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza` int(10) unsigned DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titolo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` mediumint(8) unsigned DEFAULT NULL,
  `data_creazione` int(11) DEFAULT NULL,
  `ordine` int(11) DEFAULT NULL,
  `id_nodo_parent` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `livello` tinyint(3) unsigned DEFAULT NULL,
  `versione` tinyint(3) unsigned DEFAULT NULL,
  `n_contatti` int(10) unsigned DEFAULT NULL,
  `icona` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_didascalia` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_sfondo` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correttezza` tinyint(3) unsigned DEFAULT NULL,
  `copyright` tinyint(3) unsigned DEFAULT NULL,
  `lingua` tinyint(3) NOT NULL,
  `pubblicato` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nodo`
--

LOCK TABLES `nodo` WRITE;
/*!40000 ALTER TABLE `nodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `nodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `openmeetings_room`
--

DROP TABLE IF EXISTS `openmeetings_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `openmeetings_room` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL,
  `id_istanza_corso` int(10) NOT NULL,
  `id_tutor` int(10) NOT NULL,
  `tipo_videochat` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione_videochat` text COLLATE utf8_unicode_ci NOT NULL,
  `tempo_avvio` int(11) NOT NULL,
  `tempo_fine` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`),
  KEY `id_istanza_corso` (`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `openmeetings_room`
--

LOCK TABLES `openmeetings_room` WRITE;
/*!40000 ALTER TABLE `openmeetings_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `openmeetings_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posizione`
--

DROP TABLE IF EXISTS `posizione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posizione` (
  `id_posizione` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `x0` int(11) NOT NULL DEFAULT 0,
  `y0` int(11) NOT NULL DEFAULT 0,
  `x1` int(11) NOT NULL DEFAULT 0,
  `y1` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_posizione`),
  UNIQUE KEY `posizione_coords` (`x0`,`y0`,`x1`,`y1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posizione`
--

LOCK TABLES `posizione` WRITE;
/*!40000 ALTER TABLE `posizione` DISABLE KEYS */;
/*!40000 ALTER TABLE `posizione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `risorsa_esterna`
--

DROP TABLE IF EXISTS `risorsa_esterna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risorsa_esterna` (
  `id_risorsa_ext` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `copyright` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione` text COLLATE utf8_unicode_ci NOT NULL,
  `pubblicato` tinyint(1) NOT NULL,
  `lingua` tinyint(3) NOT NULL,
  PRIMARY KEY (`id_risorsa_ext`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `risorsa_esterna`
--

LOCK TABLES `risorsa_esterna` WRITE;
/*!40000 ALTER TABLE `risorsa_esterna` DISABLE KEYS */;
/*!40000 ALTER TABLE `risorsa_esterna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `risorse_nodi`
--

DROP TABLE IF EXISTS `risorse_nodi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risorse_nodi` (
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_risorsa_ext` int(10) unsigned NOT NULL DEFAULT 0,
  `peso` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_nodo`,`id_risorsa_ext`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `risorse_nodi`
--

LOCK TABLES `risorse_nodi` WRITE;
/*!40000 ALTER TABLE `risorse_nodi` DISABLE KEYS */;
/*!40000 ALTER TABLE `risorse_nodi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessione_eguidance`
--

DROP TABLE IF EXISTS `sessione_eguidance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessione_eguidance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_tutor` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `event_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_ora` int(11) unsigned NOT NULL DEFAULT 0,
  `tipo_eguidance` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `ud_1` tinyint(3) unsigned DEFAULT 0,
  `ud_2` tinyint(3) unsigned DEFAULT 0,
  `ud_3` tinyint(3) unsigned DEFAULT 0,
  `ud_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pc_1` tinyint(3) unsigned DEFAULT 0,
  `pc_2` tinyint(3) unsigned DEFAULT 0,
  `pc_3` tinyint(3) unsigned DEFAULT 0,
  `pc_4` tinyint(3) unsigned DEFAULT 0,
  `pc_5` tinyint(3) unsigned DEFAULT 0,
  `pc_6` tinyint(3) unsigned DEFAULT 0,
  `pc_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ba_1` tinyint(3) unsigned DEFAULT 0,
  `ba_2` tinyint(3) unsigned DEFAULT 0,
  `ba_3` tinyint(3) unsigned DEFAULT 0,
  `ba_4` tinyint(3) unsigned DEFAULT 0,
  `ba_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `t_1` tinyint(3) unsigned DEFAULT 0,
  `t_2` tinyint(3) unsigned DEFAULT 0,
  `t_3` tinyint(3) unsigned DEFAULT 0,
  `t_4` tinyint(3) unsigned DEFAULT 0,
  `t_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pe_1` tinyint(3) unsigned DEFAULT 0,
  `pe_2` tinyint(3) unsigned DEFAULT 0,
  `pe_3` tinyint(3) unsigned DEFAULT 0,
  `pe_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ci_1` tinyint(3) unsigned DEFAULT 0,
  `ci_2` tinyint(3) unsigned DEFAULT 0,
  `ci_3` tinyint(3) unsigned DEFAULT 0,
  `ci_4` tinyint(3) unsigned DEFAULT 0,
  `ci_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_1` tinyint(3) unsigned DEFAULT 0,
  `m_2` tinyint(3) unsigned DEFAULT 0,
  `m_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessione_eguidance`
--

LOCK TABLES `sessione_eguidance` WRITE;
/*!40000 ALTER TABLE `sessione_eguidance` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessione_eguidance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studente`
--

DROP TABLE IF EXISTS `studente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studente` (
  `id_utente_studente` int(11) NOT NULL,
  `samplefield` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_utente_studente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studente`
--

LOCK TABLES `studente` WRITE;
/*!40000 ALTER TABLE `studente` DISABLE KEYS */;
/*!40000 ALTER TABLE `studente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor`
--

DROP TABLE IF EXISTS `tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor` (
  `id_utente_tutor` int(10) unsigned NOT NULL DEFAULT 0,
  `profilo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tariffa` decimal(7,2) unsigned NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id_utente_tutor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor`
--

LOCK TABLES `tutor` WRITE;
/*!40000 ALTER TABLE `tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_studenti`
--

DROP TABLE IF EXISTS `tutor_studenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor_studenti` (
  `id_utente_tutor` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente_tutor`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_studenti`
--

LOCK TABLES `tutor_studenti` WRITE;
/*!40000 ALTER TABLE `tutor_studenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutor_studenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente`
--

DROP TABLE IF EXISTS `utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente` (
  `id_utente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cognome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `layout` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `indirizzo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provincia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nazione` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codice_fiscale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthdate` int(12) DEFAULT NULL,
  `sesso` enum('F','M') COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stato` tinyint(3) unsigned NOT NULL,
  `lingua` tinyint(3) DEFAULT 0,
  `timezone` int(11) DEFAULT 0,
  `cap` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `matricola` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `birthcity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthprovince` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_utente`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente`
--

LOCK TABLES `utente` WRITE;
/*!40000 ALTER TABLE `utente` DISABLE KEYS */;
INSERT INTO `utente` VALUES (1,'admin','ada','2','','adminAda','4a14cd19411ae1ea8139424e89214506619eeb5f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'','','','',NULL);
/*!40000 ALTER TABLE `utente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_chatroom`
--

DROP TABLE IF EXISTS `utente_chatroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_chatroom` (
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_chatroom` int(10) unsigned NOT NULL DEFAULT 0,
  `stato_utente` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tempo_entrata` int(11) NOT NULL DEFAULT 0,
  `tempo_ultimo_evento` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente`,`id_chatroom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_chatroom`
--

LOCK TABLES `utente_chatroom` WRITE;
/*!40000 ALTER TABLE `utente_chatroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_chatroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_chatroom_log`
--

DROP TABLE IF EXISTS `utente_chatroom_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_chatroom_log` (
  `tempo` int(11) NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `azione` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_operatore` int(10) unsigned NOT NULL DEFAULT 0,
  `id_chatroom` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`tempo`,`id_utente`,`azione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_chatroom_log`
--

LOCK TABLES `utente_chatroom_log` WRITE;
/*!40000 ALTER TABLE `utente_chatroom_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_chatroom_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_log`
--

DROP TABLE IF EXISTS `utente_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_log` (
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `testo` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `utente_log_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_log`
--

LOCK TABLES `utente_log` WRITE;
/*!40000 ALTER TABLE `utente_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_messaggio_log`
--

DROP TABLE IF EXISTS `utente_messaggio_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_messaggio_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tempo` int(11) NOT NULL DEFAULT 0,
  `id_mittente` int(10) unsigned NOT NULL DEFAULT 0,
  `testo` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `titolo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `lingua` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'it',
  `id_riceventi` int(10) unsigned DEFAULT NULL,
  `flags` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`tempo`,`id_mittente`,`testo`),
  UNIQUE KEY `utente_messaggio_log_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_messaggio_log`
--

LOCK TABLES `utente_messaggio_log` WRITE;
/*!40000 ALTER TABLE `utente_messaggio_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_messaggio_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-15 10:34:24
