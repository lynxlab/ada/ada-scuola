-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: mariadb    Database: primaria_provider
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB-1:10.4.13+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ADA_moreUserFields`
--

DROP TABLE IF EXISTS `ADA_moreUserFields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADA_moreUserFields` (
  `idMoreUserFields` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `studente_id_utente_studente` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMoreUserFields`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADA_moreUserFields`
--

LOCK TABLES `ADA_moreUserFields` WRITE;
/*!40000 ALTER TABLE `ADA_moreUserFields` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADA_moreUserFields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ADA_someMoreUserFields`
--

DROP TABLE IF EXISTS `ADA_someMoreUserFields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADA_someMoreUserFields` (
  `idSomeMoreUserFields` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `foreign_key` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSomeMoreUserFields`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADA_someMoreUserFields`
--

LOCK TABLES `ADA_someMoreUserFields` WRITE;
/*!40000 ALTER TABLE `ADA_someMoreUserFields` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADA_someMoreUserFields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amministratore_corsi`
--

DROP TABLE IF EXISTS `amministratore_corsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amministratore_corsi` (
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente_amministratore` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_corso`,`id_utente_amministratore`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amministratore_corsi`
--

LOCK TABLES `amministratore_corsi` WRITE;
/*!40000 ALTER TABLE `amministratore_corsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `amministratore_corsi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amministratore_sistema`
--

DROP TABLE IF EXISTS `amministratore_sistema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amministratore_sistema` (
  `id_utente_amministratore_sist` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente_amministratore_sist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amministratore_sistema`
--

LOCK TABLES `amministratore_sistema` WRITE;
/*!40000 ALTER TABLE `amministratore_sistema` DISABLE KEYS */;
INSERT INTO `amministratore_sistema` VALUES (1);
/*!40000 ALTER TABLE `amministratore_sistema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autore`
--

DROP TABLE IF EXISTS `autore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autore` (
  `id_utente_autore` int(10) unsigned NOT NULL DEFAULT 0,
  `profilo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tariffa` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente_autore`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autore`
--

LOCK TABLES `autore` WRITE;
/*!40000 ALTER TABLE `autore` DISABLE KEYS */;
/*!40000 ALTER TABLE `autore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id_banner` int(10) NOT NULL AUTO_INCREMENT,
  `address` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_client` int(10) NOT NULL DEFAULT 0,
  `id_course` int(10) NOT NULL DEFAULT 0,
  `module` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `impressions` int(11) NOT NULL DEFAULT 0,
  `a_impressions` int(11) NOT NULL DEFAULT 0,
  `date_from` int(11) DEFAULT NULL,
  `date_to` int(11) DEFAULT NULL,
  KEY `id_banner` (`id_banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookmark`
--

DROP TABLE IF EXISTS `bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark` (
  `id_bookmark` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_utente_studente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `data` int(11) NOT NULL DEFAULT 0,
  `descrizione` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` smallint(5) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_bookmark`),
  KEY `bookmark_date` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookmark`
--

LOCK TABLES `bookmark` WRITE;
/*!40000 ALTER TABLE `bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chatroom`
--

DROP TABLE IF EXISTS `chatroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom` (
  `id_chatroom` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo_chat` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titolo_chat` text COLLATE utf8_unicode_ci NOT NULL,
  `argomento_chat` text COLLATE utf8_unicode_ci NOT NULL,
  `id_proprietario_chat` int(10) unsigned NOT NULL DEFAULT 0,
  `tempo_avvio` int(11) NOT NULL DEFAULT 0,
  `tempo_fine` int(11) NOT NULL DEFAULT 0,
  `msg_benvenuto` text COLLATE utf8_unicode_ci NOT NULL,
  `max_utenti` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_chatroom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chatroom`
--

LOCK TABLES `chatroom` WRITE;
/*!40000 ALTER TABLE `chatroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `chatroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clienti`
--

DROP TABLE IF EXISTS `clienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienti` (
  `id_client` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `clienti_id` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienti`
--

LOCK TABLES `clienti` WRITE;
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destinatari_messaggi`
--

DROP TABLE IF EXISTS `destinatari_messaggi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinatari_messaggi` (
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_messaggio` int(10) unsigned NOT NULL DEFAULT 0,
  `read_timestamp` int(11) NOT NULL DEFAULT 0,
  `deleted` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id_utente`,`id_messaggio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destinatari_messaggi`
--

LOCK TABLES `destinatari_messaggi` WRITE;
/*!40000 ALTER TABLE `destinatari_messaggi` DISABLE KEYS */;
/*!40000 ALTER TABLE `destinatari_messaggi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extended_node`
--

DROP TABLE IF EXISTS `extended_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extended_node` (
  `id_node` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `hyphenation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grammar` text COLLATE utf8_unicode_ci NOT NULL,
  `semantic` text COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `examples` text COLLATE utf8_unicode_ci NOT NULL,
  `language` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_node`
--

LOCK TABLES `extended_node` WRITE;
/*!40000 ALTER TABLE `extended_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `extended_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_esercizi`
--

DROP TABLE IF EXISTS `history_esercizi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_esercizi` (
  `ID_HISTORY_EX` int(10) NOT NULL AUTO_INCREMENT,
  `ID_UTENTE_STUDENTE` int(10) NOT NULL DEFAULT 0,
  `ID_NODO` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ID_ISTANZA_CORSO` int(10) NOT NULL DEFAULT 0,
  `DATA_VISITA` int(11) NOT NULL DEFAULT 0,
  `DATA_USCITA` int(11) DEFAULT NULL,
  `RISPOSTA_LIBERA` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENTO` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `PUNTEGGIO` smallint(4) DEFAULT NULL,
  `CORREZIONE_RISPOSTA_LIBERA` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `RIPETIBILE` smallint(1) NOT NULL DEFAULT 0,
  `ALLEGATO` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_HISTORY_EX`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_esercizi`
--

LOCK TABLES `history_esercizi` WRITE;
/*!40000 ALTER TABLE `history_esercizi` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_esercizi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_nodi`
--

DROP TABLE IF EXISTS `history_nodi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_nodi` (
  `id_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente_studente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `data_visita` int(11) NOT NULL DEFAULT 0,
  `data_uscita` int(11) NOT NULL DEFAULT 0,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `remote_address` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installation_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_from` smallint(5) unsigned DEFAULT 0,
  PRIMARY KEY (`id_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_nodi`
--

LOCK TABLES `history_nodi` WRITE;
/*!40000 ALTER TABLE `history_nodi` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_nodi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iscrizioni`
--

DROP TABLE IF EXISTS `iscrizioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iscrizioni` (
  `id_utente_studente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `livello` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `codice` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_iscrizione` int(11) DEFAULT NULL,
  `laststatusupdate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_utente_studente`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iscrizioni`
--

LOCK TABLES `iscrizioni` WRITE;
/*!40000 ALTER TABLE `iscrizioni` DISABLE KEYS */;
/*!40000 ALTER TABLE `iscrizioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `istanza_corso`
--

DROP TABLE IF EXISTS `istanza_corso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `istanza_corso` (
  `id_istanza_corso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `data_inizio` int(11) NOT NULL DEFAULT 0,
  `durata` int(10) unsigned DEFAULT NULL,
  `data_inizio_previsto` int(11) NOT NULL DEFAULT 0,
  `id_layout` int(10) unsigned NOT NULL DEFAULT 0,
  `data_fine` int(11) NOT NULL DEFAULT 0,
  `status` int(10) unsigned NOT NULL DEFAULT 0,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(7,2) NOT NULL,
  `self_instruction` tinyint(1) NOT NULL,
  `self_registration` tinyint(1) NOT NULL,
  `start_level_student` int(2) NOT NULL,
  `duration_subscription` int(3) NOT NULL,
  `open_subscription` tinyint(1) NOT NULL,
  `duration_hours` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo_servizio` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `istanza_corso`
--

LOCK TABLES `istanza_corso` WRITE;
/*!40000 ALTER TABLE `istanza_corso` DISABLE KEYS */;
/*!40000 ALTER TABLE `istanza_corso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `id_link` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_nodo_to` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_posizione` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `data_creazione` int(11) DEFAULT NULL,
  `stile` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `significato` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `azione` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_link`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link`
--

LOCK TABLES `link` WRITE;
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
/*!40000 ALTER TABLE `link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_classi`
--

DROP TABLE IF EXISTS `log_classi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_classi` (
  `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `visite` int(10) unsigned NOT NULL DEFAULT 0,
  `punti` int(10) unsigned NOT NULL DEFAULT 0,
  `esercizi` int(10) unsigned NOT NULL DEFAULT 0,
  `msg_out` int(10) unsigned NOT NULL DEFAULT 0,
  `msg_in` int(10) unsigned NOT NULL DEFAULT 0,
  `notes_in` int(10) unsigned NOT NULL DEFAULT 0,
  `notes_out` int(10) unsigned NOT NULL DEFAULT 0,
  `chat` int(10) unsigned NOT NULL DEFAULT 0,
  `bookmarks` int(10) unsigned NOT NULL DEFAULT 0,
  `indice_att` int(10) unsigned NOT NULL DEFAULT 0,
  `level` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `last_access` int(11) NOT NULL DEFAULT 0,
  `exercises_test` int(10) unsigned NOT NULL DEFAULT 0,
  `score_test` int(10) unsigned NOT NULL DEFAULT 0,
  `exercises_survey` int(10) unsigned NOT NULL DEFAULT 0,
  `score_survey` int(10) unsigned NOT NULL DEFAULT 0,
  `subscription_status` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_classi`
--

LOCK TABLES `log_classi` WRITE;
/*!40000 ALTER TABLE `log_classi` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_classi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggi`
--

DROP TABLE IF EXISTS `messaggi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggi` (
  `id_messaggio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_group` int(10) unsigned NOT NULL DEFAULT 0,
  `data_ora` int(11) NOT NULL DEFAULT 0,
  `tipo` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_mittente` int(10) unsigned DEFAULT NULL,
  `priorita` tinyint(3) unsigned DEFAULT NULL,
  `testo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `flags` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_messaggio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggi`
--

LOCK TABLES `messaggi` WRITE;
/*!40000 ALTER TABLE `messaggi` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modello_corso`
--

DROP TABLE IF EXISTS `modello_corso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modello_corso` (
  `id_corso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente_autore` int(10) unsigned NOT NULL,
  `id_layout` int(10) unsigned DEFAULT 0,
  `nome` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titolo` text COLLATE utf8_unicode_ci NOT NULL,
  `data_creazione` int(11) DEFAULT NULL,
  `data_pubblicazione` int(11) DEFAULT NULL,
  `descrizione` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nodo_iniziale` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_nodo_toc` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `media_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `static_mode` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `id_lingua` tinyint(3) unsigned NOT NULL,
  `crediti` tinyint(3) NOT NULL DEFAULT 1,
  `duration_hours` int(10) unsigned NOT NULL DEFAULT 0,
  `tipo_servizio` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_corso`),
  UNIQUE KEY `modello_corso_nome` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modello_corso`
--

LOCK TABLES `modello_corso` WRITE;
/*!40000 ALTER TABLE `modello_corso` DISABLE KEYS */;
/*!40000 ALTER TABLE `modello_corso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_badges_badges`
--

DROP TABLE IF EXISTS `module_badges_badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_badges_badges` (
  `uuid_bin` binary(16) NOT NULL,
  `name` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`uuid_bin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_badges_badges`
--

LOCK TABLES `module_badges_badges` WRITE;
/*!40000 ALTER TABLE `module_badges_badges` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_badges_badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_badges_course_badges`
--

DROP TABLE IF EXISTS `module_badges_course_badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_badges_course_badges` (
  `badge_uuid_bin` binary(16) NOT NULL,
  `id_corso` int(10) unsigned DEFAULT NULL,
  `id_conditionset` int(10) unsigned NOT NULL,
  UNIQUE KEY `course_badges_idx` (`badge_uuid_bin`,`id_corso`,`id_conditionset`) USING BTREE,
  KEY `fk_badge_uuid` (`badge_uuid_bin`),
  KEY `fk_id_conditionset` (`id_conditionset`),
  CONSTRAINT `fk_badge_uuid` FOREIGN KEY (`badge_uuid_bin`) REFERENCES `module_badges_badges` (`uuid_bin`) ON DELETE CASCADE,
  CONSTRAINT `fk_id_conditionset` FOREIGN KEY (`id_conditionset`) REFERENCES `module_complete_conditionset` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_badges_course_badges`
--

LOCK TABLES `module_badges_course_badges` WRITE;
/*!40000 ALTER TABLE `module_badges_course_badges` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_badges_course_badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_badges_rewarded_badges`
--

DROP TABLE IF EXISTS `module_badges_rewarded_badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_badges_rewarded_badges` (
  `uuid_bin` binary(16) NOT NULL,
  `badge_uuid_bin` binary(16) NOT NULL,
  `issuedOn` int(10) unsigned NOT NULL,
  `approved` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `notified` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL,
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  PRIMARY KEY (`uuid_bin`),
  UNIQUE KEY `unique_badge_student_course` (`badge_uuid_bin`,`id_utente`,`id_corso`,`id_istanza_corso`),
  KEY `fk_rewarded_badge_uuid` (`badge_uuid_bin`),
  KEY `fk_rewarded_badge_id_corso` (`id_corso`),
  KEY `fk_rewarded_badge_id_istanza_corso` (`id_istanza_corso`),
  KEY `fk_rewarded_badge_id_utente` (`id_utente`),
  CONSTRAINT `fk_rewarded_badge_id_corso` FOREIGN KEY (`id_corso`) REFERENCES `modello_corso` (`id_corso`) ON DELETE CASCADE,
  CONSTRAINT `fk_rewarded_badge_id_istanza_corso` FOREIGN KEY (`id_istanza_corso`) REFERENCES `istanza_corso` (`id_istanza_corso`) ON DELETE CASCADE,
  CONSTRAINT `fk_rewarded_badge_id_utente` FOREIGN KEY (`id_utente`) REFERENCES `utente` (`id_utente`) ON DELETE CASCADE,
  CONSTRAINT `fk_rewarded_badge_uuid` FOREIGN KEY (`badge_uuid_bin`) REFERENCES `module_badges_badges` (`uuid_bin`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_badges_rewarded_badges`
--

LOCK TABLES `module_badges_rewarded_badges` WRITE;
/*!40000 ALTER TABLE `module_badges_rewarded_badges` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_badges_rewarded_badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classagenda_calendars`
--

DROP TABLE IF EXISTS `module_classagenda_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classagenda_calendars` (
  `module_classagenda_calendars_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start` int(11) unsigned NOT NULL,
  `end` int(11) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `id_classroom` int(10) unsigned DEFAULT NULL,
  `id_utente_tutor` int(10) unsigned NOT NULL,
  PRIMARY KEY (`module_classagenda_calendars_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classagenda_calendars`
--

LOCK TABLES `module_classagenda_calendars` WRITE;
/*!40000 ALTER TABLE `module_classagenda_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classagenda_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classagenda_reminder_history`
--

DROP TABLE IF EXISTS `module_classagenda_reminder_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classagenda_reminder_history` (
  `module_classagenda_reminder_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_classagenda_calendars_id` int(10) unsigned NOT NULL,
  `html` text COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` int(11) NOT NULL,
  PRIMARY KEY (`module_classagenda_reminder_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classagenda_reminder_history`
--

LOCK TABLES `module_classagenda_reminder_history` WRITE;
/*!40000 ALTER TABLE `module_classagenda_reminder_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classagenda_reminder_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classagenda_rollcall`
--

DROP TABLE IF EXISTS `module_classagenda_rollcall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classagenda_rollcall` (
  `module_classagenda_rollcall_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente_studente` int(10) NOT NULL,
  `module_classagenda_calendars_id` int(10) NOT NULL,
  `entertime` int(11) unsigned NOT NULL,
  `exittime` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`module_classagenda_rollcall_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classagenda_rollcall`
--

LOCK TABLES `module_classagenda_rollcall` WRITE;
/*!40000 ALTER TABLE `module_classagenda_rollcall` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classagenda_rollcall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_budget_instance`
--

DROP TABLE IF EXISTS `module_classbudget_budget_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_budget_instance` (
  `budget_instance_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `budget` decimal(8,2) unsigned DEFAULT NULL,
  `references` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`budget_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_budget_instance`
--

LOCK TABLES `module_classbudget_budget_instance` WRITE;
/*!40000 ALTER TABLE `module_classbudget_budget_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_budget_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_cost_classroom`
--

DROP TABLE IF EXISTS `module_classbudget_cost_classroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_cost_classroom` (
  `cost_classroom_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_classroom` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `hourly_rate` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`cost_classroom_id`),
  UNIQUE KEY `id_classroom_instance` (`id_classroom`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_cost_classroom`
--

LOCK TABLES `module_classbudget_cost_classroom` WRITE;
/*!40000 ALTER TABLE `module_classbudget_cost_classroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_cost_classroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_cost_item`
--

DROP TABLE IF EXISTS `module_classbudget_cost_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_cost_item` (
  `cost_item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `applied_to` int(2) DEFAULT NULL,
  PRIMARY KEY (`cost_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_cost_item`
--

LOCK TABLES `module_classbudget_cost_item` WRITE;
/*!40000 ALTER TABLE `module_classbudget_cost_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_cost_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classbudget_cost_tutor`
--

DROP TABLE IF EXISTS `module_classbudget_cost_tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classbudget_cost_tutor` (
  `cost_tutor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tutor` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL,
  `hourly_rate` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`cost_tutor_id`),
  UNIQUE KEY `id_classroom_instance` (`id_tutor`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classbudget_cost_tutor`
--

LOCK TABLES `module_classbudget_cost_tutor` WRITE;
/*!40000 ALTER TABLE `module_classbudget_cost_tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classbudget_cost_tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classroom_classrooms`
--

DROP TABLE IF EXISTS `module_classroom_classrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classroom_classrooms` (
  `id_classroom` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_venue` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seats` int(4) unsigned DEFAULT NULL,
  `computers` tinyint(4) unsigned DEFAULT NULL,
  `internet` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `wifi` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `projector` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `mobility_impaired` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `hourly_rate` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id_classroom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classroom_classrooms`
--

LOCK TABLES `module_classroom_classrooms` WRITE;
/*!40000 ALTER TABLE `module_classroom_classrooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classroom_classrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_classroom_venues`
--

DROP TABLE IF EXISTS `module_classroom_venues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_classroom_venues` (
  `id_venue` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressline1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addressline2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_venue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_classroom_venues`
--

LOCK TABLES `module_classroom_venues` WRITE;
/*!40000 ALTER TABLE `module_classroom_venues` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_classroom_venues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_complete_conditionset`
--

DROP TABLE IF EXISTS `module_complete_conditionset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_complete_conditionset` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descrizione` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_complete_conditionset`
--

LOCK TABLES `module_complete_conditionset` WRITE;
/*!40000 ALTER TABLE `module_complete_conditionset` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_complete_conditionset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_complete_conditionset_course`
--

DROP TABLE IF EXISTS `module_complete_conditionset_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_complete_conditionset_course` (
  `id_conditionset` int(10) unsigned NOT NULL COMMENT 'id of the completeset rule',
  `id_course` int(10) unsigned NOT NULL COMMENT 'id of the course linked to the completeset rule',
  PRIMARY KEY (`id_conditionset`,`id_course`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_complete_conditionset_course`
--

LOCK TABLES `module_complete_conditionset_course` WRITE;
/*!40000 ALTER TABLE `module_complete_conditionset_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_complete_conditionset_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_complete_operations`
--

DROP TABLE IF EXISTS `module_complete_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_complete_operations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_conditionset` int(11) NOT NULL,
  `operator` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operand1` text COLLATE utf8_unicode_ci NOT NULL,
  `operand2` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` int(11) NOT NULL COMMENT 'this is called priority but it''s used to tell in which column of the UI is the conditionSet',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_complete_operations`
--

LOCK TABLES `module_complete_operations` WRITE;
/*!40000 ALTER TABLE `module_complete_operations` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_complete_operations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_forkedpaths_history`
--

DROP TABLE IF EXISTS `module_forkedpaths_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_forkedpaths_history` (
  `module_forkedpaths_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `courseInstanceId` int(10) unsigned NOT NULL,
  `nodeFrom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nodeTo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `userLevelFrom` tinyint(3) unsigned NOT NULL,
  `userLevelTo` tinyint(3) unsigned NOT NULL,
  `saveTS` int(10) unsigned NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`module_forkedpaths_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_forkedpaths_history`
--

LOCK TABLES `module_forkedpaths_history` WRITE;
/*!40000 ALTER TABLE `module_forkedpaths_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_forkedpaths_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_formmail_helptype`
--

DROP TABLE IF EXISTS `module_formmail_helptype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_formmail_helptype` (
  `module_formmail_helptype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recipient` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`module_formmail_helptype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_formmail_helptype`
--

LOCK TABLES `module_formmail_helptype` WRITE;
/*!40000 ALTER TABLE `module_formmail_helptype` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_formmail_helptype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_formmail_history`
--

DROP TABLE IF EXISTS `module_formmail_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_formmail_history` (
  `module_formmail_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL,
  `module_formmail_helptype_id` int(10) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `msgbody` text COLLATE utf8_unicode_ci NOT NULL,
  `attachments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `selfSent` tinyint(1) unsigned NOT NULL,
  `sentOK` tinyint(1) unsigned NOT NULL,
  `sentTimestamp` int(10) NOT NULL,
  PRIMARY KEY (`module_formmail_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_formmail_history`
--

LOCK TABLES `module_formmail_history` WRITE;
/*!40000 ALTER TABLE `module_formmail_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_formmail_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_policy_content`
--

DROP TABLE IF EXISTS `module_gdpr_policy_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_policy_content` (
  `policy_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tester_pointer` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mandatory` tinyint(3) unsigned DEFAULT 0,
  `isPublished` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `lastEditTS` int(11) NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`policy_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_policy_content`
--

LOCK TABLES `module_gdpr_policy_content` WRITE;
/*!40000 ALTER TABLE `module_gdpr_policy_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_policy_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_policy_utente`
--

DROP TABLE IF EXISTS `module_gdpr_policy_utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_policy_utente` (
  `id_utente` int(10) unsigned NOT NULL,
  `id_policy` int(10) unsigned NOT NULL,
  `acceptedVersion` int(11) unsigned NOT NULL,
  `lastmodTS` int(11) unsigned NOT NULL,
  `isAccepted` tinyint(3) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente`,`id_policy`,`acceptedVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_policy_utente`
--

LOCK TABLES `module_gdpr_policy_utente` WRITE;
/*!40000 ALTER TABLE `module_gdpr_policy_utente` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_policy_utente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_requestTypes`
--

DROP TABLE IF EXISTS `module_gdpr_requestTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_requestTypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_requestTypes`
--

LOCK TABLES `module_gdpr_requestTypes` WRITE;
/*!40000 ALTER TABLE `module_gdpr_requestTypes` DISABLE KEYS */;
INSERT INTO `module_gdpr_requestTypes` VALUES (1,4,'Cancellazione','{\"confirmhandle\":true}'),(2,1,'Accesso',NULL),(3,2,'Modifica',NULL),(4,3,'Limita','{\"confirmhandle\":true}'),(5,5,'Opposizione',NULL);
/*!40000 ALTER TABLE `module_gdpr_requestTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_requests`
--

DROP TABLE IF EXISTS `module_gdpr_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_requests` (
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `generatedBy` int(11) NOT NULL,
  `generatedTs` int(11) NOT NULL,
  `confirmedTs` int(11) DEFAULT NULL,
  `closedBy` int(11) DEFAULT NULL,
  `closedTs` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `selfOpened` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_requests`
--

LOCK TABLES `module_gdpr_requests` WRITE;
/*!40000 ALTER TABLE `module_gdpr_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_userTypes`
--

DROP TABLE IF EXISTS `module_gdpr_userTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_userTypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_userTypes`
--

LOCK TABLES `module_gdpr_userTypes` WRITE;
/*!40000 ALTER TABLE `module_gdpr_userTypes` DISABLE KEYS */;
INSERT INTO `module_gdpr_userTypes` VALUES (1,'Manager'),(2,'Nessuno');
/*!40000 ALTER TABLE `module_gdpr_userTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_gdpr_users`
--

DROP TABLE IF EXISTS `module_gdpr_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_gdpr_users` (
  `id_utente` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_utente`,`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_gdpr_users`
--

LOCK TABLES `module_gdpr_users` WRITE;
/*!40000 ALTER TABLE `module_gdpr_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_gdpr_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_history_login`
--

DROP TABLE IF EXISTS `module_login_history_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_history_login` (
  `id_utente` int(10) unsigned NOT NULL,
  `date` int(11) NOT NULL,
  `module_login_providers_id` int(5) unsigned NOT NULL,
  `successfulOptionsID` int(5) unsigned NOT NULL,
  PRIMARY KEY (`id_utente`,`date`,`module_login_providers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_history_login`
--

LOCK TABLES `module_login_history_login` WRITE;
/*!40000 ALTER TABLE `module_login_history_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_login_history_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_options`
--

DROP TABLE IF EXISTS `module_login_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_options` (
  `module_login_providers_options_id` int(5) unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`key`,`module_login_providers_options_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_options`
--

LOCK TABLES `module_login_options` WRITE;
/*!40000 ALTER TABLE `module_login_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_login_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_providers`
--

DROP TABLE IF EXISTS `module_login_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_providers` (
  `module_login_providers_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `className` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `buttonLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayOrder` int(4) unsigned NOT NULL,
  PRIMARY KEY (`module_login_providers_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_providers`
--

LOCK TABLES `module_login_providers` WRITE;
/*!40000 ALTER TABLE `module_login_providers` DISABLE KEYS */;
INSERT INTO `module_login_providers` VALUES (1,'adaLogin','Ada',1,'Accedi',1);
/*!40000 ALTER TABLE `module_login_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_login_providers_options`
--

DROP TABLE IF EXISTS `module_login_providers_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_login_providers_options` (
  `module_login_providers_options_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `module_login_providers_id` int(5) unsigned NOT NULL,
  `order` int(5) unsigned NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`module_login_providers_options_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_login_providers_options`
--

LOCK TABLES `module_login_providers_options` WRITE;
/*!40000 ALTER TABLE `module_login_providers_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_login_providers_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_newsletter_history`
--

DROP TABLE IF EXISTS `module_newsletter_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_newsletter_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_newsletter` int(10) unsigned DEFAULT NULL,
  `filter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datesent` int(11) NOT NULL,
  `recipientscount` int(6) unsigned NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_newsletter_history`
--

LOCK TABLES `module_newsletter_history` WRITE;
/*!40000 ALTER TABLE `module_newsletter_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_newsletter_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_newsletter_newsletters`
--

DROP TABLE IF EXISTS `module_newsletter_newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_newsletter_newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmltext` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `plaintext` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `draft` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_newsletter_newsletters`
--

LOCK TABLES `module_newsletter_newsletters` WRITE;
/*!40000 ALTER TABLE `module_newsletter_newsletters` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_newsletter_newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_studentsgroups_groups`
--

DROP TABLE IF EXISTS `module_studentsgroups_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_studentsgroups_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customField0` int(10) unsigned DEFAULT NULL COMMENT 'application managed',
  `customField1` int(10) unsigned DEFAULT NULL COMMENT 'application managed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_studentsgroups_groups`
--

LOCK TABLES `module_studentsgroups_groups` WRITE;
/*!40000 ALTER TABLE `module_studentsgroups_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_studentsgroups_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_studentsgroups_groups_utente`
--

DROP TABLE IF EXISTS `module_studentsgroups_groups_utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_studentsgroups_groups_utente` (
  `group_id` int(10) unsigned NOT NULL,
  `utente_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `group_utente_idx` (`group_id`,`utente_id`),
  KEY `fk_studentsgroups_utente` (`utente_id`),
  CONSTRAINT `fk_studentsgroups_groups` FOREIGN KEY (`group_id`) REFERENCES `module_studentsgroups_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_studentsgroups_utente` FOREIGN KEY (`utente_id`) REFERENCES `utente` (`id_utente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_studentsgroups_groups_utente`
--

LOCK TABLES `module_studentsgroups_groups_utente` WRITE;
/*!40000 ALTER TABLE `module_studentsgroups_groups_utente` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_studentsgroups_groups_utente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_course_survey`
--

DROP TABLE IF EXISTS `module_test_course_survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_course_survey` (
  `id_corso` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  `id_nodo` varchar(64) NOT NULL,
  PRIMARY KEY (`id_corso`,`id_test`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_course_survey`
--

LOCK TABLES `module_test_course_survey` WRITE;
/*!40000 ALTER TABLE `module_test_course_survey` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_course_survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_history_answer`
--

DROP TABLE IF EXISTS `module_test_history_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_history_answer` (
  `id_answer` int(10) NOT NULL AUTO_INCREMENT,
  `id_history_test` int(10) unsigned NOT NULL,
  `id_utente` int(10) NOT NULL DEFAULT 0,
  `id_topic` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'id of relative topic test node',
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL COMMENT 'id of relative question test node',
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) DEFAULT NULL COMMENT 'id of relative course instance',
  `risposta` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'student''s answer (a serialized array)',
  `commento` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tutor''s comment',
  `punteggio` smallint(4) DEFAULT NULL,
  `correzione_risposta` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `allegato` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `data` int(10) NOT NULL,
  PRIMARY KEY (`id_answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_history_answer`
--

LOCK TABLES `module_test_history_answer` WRITE;
/*!40000 ALTER TABLE `module_test_history_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_history_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_history_test`
--

DROP TABLE IF EXISTS `module_test_history_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_history_test` (
  `id_history_test` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_corso` int(10) unsigned NOT NULL,
  `id_istanza_corso` int(10) unsigned DEFAULT NULL,
  `id_nodo` int(10) NOT NULL,
  `data_inizio` int(10) NOT NULL,
  `data_fine` int(10) NOT NULL,
  `punteggio_realizzato` int(10) unsigned NOT NULL DEFAULT 0,
  `ripetibile` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `punteggio_minimo_barriera` int(10) unsigned NOT NULL DEFAULT 0,
  `livello_raggiunto` int(10) unsigned DEFAULT NULL,
  `consegnato` tinyint(1) NOT NULL DEFAULT 0,
  `tempo_scaduto` tinyint(1) NOT NULL DEFAULT 0,
  `domande` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_history_test`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_history_test`
--

LOCK TABLES `module_test_history_test` WRITE;
/*!40000 ALTER TABLE `module_test_history_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_history_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_test_nodes`
--

DROP TABLE IF EXISTS `module_test_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_test_nodes` (
  `id_nodo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_posizione` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza` int(10) unsigned DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consegna` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `testo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` mediumint(8) unsigned DEFAULT NULL,
  `data_creazione` int(10) DEFAULT NULL,
  `ordine` int(10) DEFAULT NULL,
  `id_nodo_parent` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nodo_radice` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nodo_riferimento` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `livello` int(10) unsigned DEFAULT 0,
  `versione` int(10) unsigned NOT NULL DEFAULT 0,
  `n_contatti` int(10) unsigned NOT NULL DEFAULT 0,
  `icona` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_didascalia` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_sfondo` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correttezza` tinyint(3) unsigned DEFAULT NULL,
  `copyright` tinyint(3) unsigned DEFAULT NULL,
  `didascalia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `durata` int(10) DEFAULT NULL,
  `titolo_dragdrop` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_test_nodes`
--

LOCK TABLES `module_test_nodes` WRITE;
/*!40000 ALTER TABLE `module_test_nodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_test_nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nodo`
--

DROP TABLE IF EXISTS `nodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodo` (
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_posizione` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza` int(10) unsigned DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titolo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` mediumint(8) unsigned DEFAULT NULL,
  `data_creazione` int(11) DEFAULT NULL,
  `ordine` int(11) DEFAULT NULL,
  `id_nodo_parent` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `livello` tinyint(3) unsigned DEFAULT NULL,
  `versione` tinyint(3) unsigned DEFAULT NULL,
  `n_contatti` int(10) unsigned DEFAULT NULL,
  `icona` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_didascalia` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colore_sfondo` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correttezza` tinyint(3) unsigned DEFAULT NULL,
  `copyright` tinyint(3) unsigned DEFAULT NULL,
  `lingua` tinyint(3) NOT NULL,
  `pubblicato` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nodo`
--

LOCK TABLES `nodo` WRITE;
/*!40000 ALTER TABLE `nodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `nodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `openmeetings_room`
--

DROP TABLE IF EXISTS `openmeetings_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `openmeetings_room` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL,
  `id_istanza_corso` int(10) NOT NULL,
  `id_tutor` int(10) NOT NULL,
  `tipo_videochat` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione_videochat` text COLLATE utf8_unicode_ci NOT NULL,
  `tempo_avvio` int(11) NOT NULL,
  `tempo_fine` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`),
  KEY `id_istanza_corso` (`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `openmeetings_room`
--

LOCK TABLES `openmeetings_room` WRITE;
/*!40000 ALTER TABLE `openmeetings_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `openmeetings_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posizione`
--

DROP TABLE IF EXISTS `posizione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posizione` (
  `id_posizione` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `x0` int(11) NOT NULL DEFAULT 0,
  `y0` int(11) NOT NULL DEFAULT 0,
  `x1` int(11) NOT NULL DEFAULT 0,
  `y1` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_posizione`),
  UNIQUE KEY `posizione_coords` (`x0`,`y0`,`x1`,`y1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posizione`
--

LOCK TABLES `posizione` WRITE;
/*!40000 ALTER TABLE `posizione` DISABLE KEYS */;
/*!40000 ALTER TABLE `posizione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `risorsa_esterna`
--

DROP TABLE IF EXISTS `risorsa_esterna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risorsa_esterna` (
  `id_risorsa_ext` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `copyright` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione` text COLLATE utf8_unicode_ci NOT NULL,
  `pubblicato` tinyint(1) NOT NULL,
  `lingua` tinyint(3) NOT NULL,
  PRIMARY KEY (`id_risorsa_ext`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `risorsa_esterna`
--

LOCK TABLES `risorsa_esterna` WRITE;
/*!40000 ALTER TABLE `risorsa_esterna` DISABLE KEYS */;
/*!40000 ALTER TABLE `risorsa_esterna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `risorse_nodi`
--

DROP TABLE IF EXISTS `risorse_nodi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risorse_nodi` (
  `id_nodo` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `id_risorsa_ext` int(10) unsigned NOT NULL DEFAULT 0,
  `peso` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_nodo`,`id_risorsa_ext`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `risorse_nodi`
--

LOCK TABLES `risorse_nodi` WRITE;
/*!40000 ALTER TABLE `risorse_nodi` DISABLE KEYS */;
/*!40000 ALTER TABLE `risorse_nodi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessione_eguidance`
--

DROP TABLE IF EXISTS `sessione_eguidance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessione_eguidance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_tutor` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `event_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_ora` int(11) unsigned NOT NULL DEFAULT 0,
  `tipo_eguidance` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `ud_1` tinyint(3) unsigned DEFAULT 0,
  `ud_2` tinyint(3) unsigned DEFAULT 0,
  `ud_3` tinyint(3) unsigned DEFAULT 0,
  `ud_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pc_1` tinyint(3) unsigned DEFAULT 0,
  `pc_2` tinyint(3) unsigned DEFAULT 0,
  `pc_3` tinyint(3) unsigned DEFAULT 0,
  `pc_4` tinyint(3) unsigned DEFAULT 0,
  `pc_5` tinyint(3) unsigned DEFAULT 0,
  `pc_6` tinyint(3) unsigned DEFAULT 0,
  `pc_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ba_1` tinyint(3) unsigned DEFAULT 0,
  `ba_2` tinyint(3) unsigned DEFAULT 0,
  `ba_3` tinyint(3) unsigned DEFAULT 0,
  `ba_4` tinyint(3) unsigned DEFAULT 0,
  `ba_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `t_1` tinyint(3) unsigned DEFAULT 0,
  `t_2` tinyint(3) unsigned DEFAULT 0,
  `t_3` tinyint(3) unsigned DEFAULT 0,
  `t_4` tinyint(3) unsigned DEFAULT 0,
  `t_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pe_1` tinyint(3) unsigned DEFAULT 0,
  `pe_2` tinyint(3) unsigned DEFAULT 0,
  `pe_3` tinyint(3) unsigned DEFAULT 0,
  `pe_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ci_1` tinyint(3) unsigned DEFAULT 0,
  `ci_2` tinyint(3) unsigned DEFAULT 0,
  `ci_3` tinyint(3) unsigned DEFAULT 0,
  `ci_4` tinyint(3) unsigned DEFAULT 0,
  `ci_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_1` tinyint(3) unsigned DEFAULT 0,
  `m_2` tinyint(3) unsigned DEFAULT 0,
  `m_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_comments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessione_eguidance`
--

LOCK TABLES `sessione_eguidance` WRITE;
/*!40000 ALTER TABLE `sessione_eguidance` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessione_eguidance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studente`
--

DROP TABLE IF EXISTS `studente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studente` (
  `id_utente_studente` int(11) NOT NULL,
  `samplefield` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_utente_studente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studente`
--

LOCK TABLES `studente` WRITE;
/*!40000 ALTER TABLE `studente` DISABLE KEYS */;
/*!40000 ALTER TABLE `studente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor`
--

DROP TABLE IF EXISTS `tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor` (
  `id_utente_tutor` int(10) unsigned NOT NULL DEFAULT 0,
  `profilo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tariffa` decimal(7,2) unsigned NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id_utente_tutor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor`
--

LOCK TABLES `tutor` WRITE;
/*!40000 ALTER TABLE `tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_studenti`
--

DROP TABLE IF EXISTS `tutor_studenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor_studenti` (
  `id_utente_tutor` int(10) unsigned NOT NULL DEFAULT 0,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente_tutor`,`id_istanza_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_studenti`
--

LOCK TABLES `tutor_studenti` WRITE;
/*!40000 ALTER TABLE `tutor_studenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutor_studenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente`
--

DROP TABLE IF EXISTS `utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente` (
  `id_utente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cognome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `layout` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `indirizzo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provincia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nazione` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codice_fiscale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthdate` int(12) DEFAULT NULL,
  `sesso` enum('F','M') COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stato` tinyint(3) unsigned NOT NULL,
  `lingua` tinyint(3) DEFAULT 0,
  `timezone` int(11) DEFAULT 0,
  `cap` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `matricola` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `birthcity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthprovince` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_utente`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente`
--

LOCK TABLES `utente` WRITE;
/*!40000 ALTER TABLE `utente` DISABLE KEYS */;
INSERT INTO `utente` VALUES (1,'admin','ada','2','','adminAda','4a14cd19411ae1ea8139424e89214506619eeb5f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'','','','',NULL);
/*!40000 ALTER TABLE `utente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_chatroom`
--

DROP TABLE IF EXISTS `utente_chatroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_chatroom` (
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `id_chatroom` int(10) unsigned NOT NULL DEFAULT 0,
  `stato_utente` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tempo_entrata` int(11) NOT NULL DEFAULT 0,
  `tempo_ultimo_evento` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_utente`,`id_chatroom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_chatroom`
--

LOCK TABLES `utente_chatroom` WRITE;
/*!40000 ALTER TABLE `utente_chatroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_chatroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_chatroom_log`
--

DROP TABLE IF EXISTS `utente_chatroom_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_chatroom_log` (
  `tempo` int(11) NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `azione` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_operatore` int(10) unsigned NOT NULL DEFAULT 0,
  `id_chatroom` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`tempo`,`id_utente`,`azione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_chatroom_log`
--

LOCK TABLES `utente_chatroom_log` WRITE;
/*!40000 ALTER TABLE `utente_chatroom_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_chatroom_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_log`
--

DROP TABLE IF EXISTS `utente_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_log` (
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `id_utente` int(10) unsigned NOT NULL DEFAULT 0,
  `data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `testo` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `utente_log_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_log`
--

LOCK TABLES `utente_log` WRITE;
/*!40000 ALTER TABLE `utente_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente_messaggio_log`
--

DROP TABLE IF EXISTS `utente_messaggio_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente_messaggio_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tempo` int(11) NOT NULL DEFAULT 0,
  `id_mittente` int(10) unsigned NOT NULL DEFAULT 0,
  `testo` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `titolo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_istanza_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `id_corso` int(10) unsigned NOT NULL DEFAULT 0,
  `lingua` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'it',
  `id_riceventi` int(10) unsigned DEFAULT NULL,
  `flags` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`tempo`,`id_mittente`,`testo`),
  UNIQUE KEY `utente_messaggio_log_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente_messaggio_log`
--

LOCK TABLES `utente_messaggio_log` WRITE;
/*!40000 ALTER TABLE `utente_messaggio_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `utente_messaggio_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-15 10:33:59
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Mag 14, 2020 alle 20:15
-- Versione del server: 10.3.15-MariaDB
-- Versione PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `commonDB`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `menu_items`
--

CREATE TABLE `menu_items` (
  `item_id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extraHTML` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_size` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `href_properties` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `href_prefix` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `href_path` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `href_paramlist` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `extraClass` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `groupRight` int(1) NOT NULL DEFAULT 0,
  `specialItem` int(1) NOT NULL DEFAULT 0,
  `order` int(3) UNSIGNED NOT NULL DEFAULT 0,
  `enabledON` varchar(10240) COLLATE utf8_unicode_ci NOT NULL DEFAULT '%ALWAYS%'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `menu_items`
--

INSERT INTO `menu_items` (`item_id`, `label`, `extraHTML`, `icon`, `icon_size`, `href_properties`, `href_prefix`, `href_path`, `href_paramlist`, `extraClass`, `groupRight`, `specialItem`, `order`, `enabledON`) VALUES
(1, 'home', NULL, 'home', 'large', NULL, '%HTTP_ROOT_DIR%/browsing', 'user.php', NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(2, 'Comunica', '<div class=\"ui label\" id=\"msglabel\" style=\"display:none;\">             <i class=\"mail small icon\"></i><span id=\"unreadmsgbadge\"></span>         </div>', 'comment', 'large', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, '%ALWAYS%'),
(3, 'Messaggeria', NULL, 'mail', NULL, '{\"onclick\":\"openMessenger(\'/comunica/list_messages.php\',800,600);\"}', NULL, NULL, NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(4, 'Forum', NULL, 'community basic', NULL, NULL, '%HTTP_ROOT_DIR%/browsing', 'main_index.php?op=forum', '', NULL, 0, 0, 10, '%ALWAYS%'),
(9, 'Esci', NULL, 'sign out', 'large', NULL, '%HTTP_ROOT_DIR%', 'logout.php', NULL, NULL, 1, 0, 500, '$sess_id_user'),
(10, 'cerca', '<div class=\"ui search small icon input\">\r\n<span><i18n>Cerca</i18n></span>\r\n<input type=\"text\" placeholder=\"<i18n>Cerca</i18n>...\">\r\n        <i class=\"search link icon\"></i>\r\n</div>', NULL, NULL, NULL, NULL, NULL, NULL, 'searchItem', 1, 1, 0, '%ALWAYS%'),
(11, 'Naviga', NULL, 'globe', 'large', '{\"onclick\":\"javascript: navigationPanelToggle();\"}', NULL, NULL, NULL, 'toggle navpanel', 0, 0, 25, '%ALWAYS%'),
(14, 'registrati', NULL, 'sign in', 'large', NULL, '%HTTP_ROOT_DIR%/browsing', 'registration.php', NULL, NULL, 0, 0, 0, '%NEVER%\r\n'),
(15, 'corsi', NULL, 'book', 'large', NULL, '%HTTP_ROOT_DIR%', 'info.php', NULL, NULL, 0, 0, 20, '%NEVER%'),
(16, 'help', NULL, 'question', 'large', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '{\r\n\"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}\r\n'),
(17, 'informazioni', NULL, 'info', NULL, '{\"target\":\"_blank\"}', '%HTTP_ROOT_DIR%', 'help.php', '', NULL, 0, 0, 0, '%ALWAYS%'),
(18, 'credits', NULL, 'trophy', NULL, NULL, '%HTTP_ROOT_DIR%', 'credits.php', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(20, 'strumenti', NULL, 'wrench', 'large', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, '%ALWAYS%'),
(21, 'agenda', NULL, 'calendar', NULL, '{\"onclick\":\"openMessenger(\'/comunica/list_events.php\',800,600);\"}', NULL, NULL, NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(22, 'agisci', NULL, 'pencil', 'large', NULL, NULL, NULL, NULL, NULL, 0, 0, 15, '%ALWAYS%'),
(23, 'modifica profilo', NULL, 'user', NULL, NULL, NULL, '<template_field class=\"template_field\" name=\"edit_profile\">edit_profile</template_field>', 'self_instruction', NULL, 0, 0, 0, '{\r\n \"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}'),
(24, '<template_field class=\"template_field\" name=\"user_name\">user_name</template_field>', NULL, 'user', 'large', NULL, NULL, NULL, NULL, 'userpopup', 1, 1, 495, '$sess_id_user'),
(25, 'chat', NULL, 'chat', NULL, '{\"target\":\"_blank\"}', '%HTTP_ROOT_DIR%/comunica', 'chat.php', 'id_room,id_course', NULL, 0, 0, 15, '$com_enabled'),
(26, 'video conference', NULL, 'facetime video', NULL, '{\"target\":\"_blank\"}', '%HTTP_ROOT_DIR%/comunica', 'videochat.php', NULL, NULL, 0, 0, 20, '$com_enabled'),
(27, 'collabora', NULL, 'share', NULL, NULL, '%HTTP_ROOT_DIR%/browsing', 'download.php', NULL, NULL, 0, 0, 25, '%ALWAYS%'),
(28, 'diario', NULL, 'empty calendar', NULL, NULL, '%HTTP_ROOT_DIR%/browsing', 'mylog.php', NULL, NULL, 0, 0, 10, '%ALWAYS%'),
(29, 'cronologia', NULL, 'time basic', NULL, NULL, '%HTTP_ROOT_DIR%/browsing', 'history.php', NULL, NULL, 0, 0, 15, '%ALWAYS%'),
(30, 'storico esercizi', NULL, 'chart basic', NULL, NULL, '%HTTP_ROOT_DIR%/browsing', 'exercise_history.php', 'id_course_instance', NULL, 0, 0, 20, '{\r\n \"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}'),
(31, 'storico test', NULL, 'bar chart', NULL, NULL, '%MODULES_TEST_HTTP%', 'history.php', '<template_field class=\"template_field\" name=\"test_history\">test_history</template_field>', NULL, 0, 0, 25, '{\r\n\"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}\r\n\r\n'),
(32, 'storico sondaggi', NULL, 'edit', NULL, NULL, '%MODULES_TEST_HTTP%', 'history.php?op=survey', 'id_course, id_course_instance', NULL, 0, 0, 30, '{\r\n\"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}\r\n\r\n'),
(33, 'stampa', NULL, 'print', NULL, '{\"target\":\"_blank\"}', '%HTTP_ROOT_DIR%/browsing', 'print.php', 'id_node', NULL, 0, 0, 35, '%ALWAYS%'),
(34, 'invia un file', NULL, 'upload disk', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'upload.php', NULL, NULL, 0, 0, 10, '$com_enabled'),
(35, 'codici iscrizione studenti', NULL, 'code', NULL, NULL, '%MODULES_CODEMAN_HTTP%', NULL, NULL, NULL, 0, 0, 30, '%MODULES_CODEMAN%'),
(102, 'lista tutor', NULL, 'basic users', '', NULL, '%HTTP_ROOT_DIR%/switcher', 'list_users.php?list=tutors', NULL, NULL, 0, 0, 10, '%ALWAYS%'),
(37, 'indietro', NULL, 'circle left', 'large', NULL, NULL, '<template_field class=\"template_field\" name=\"go_back\">go_back</template_field>', NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(38, 'modifica <template_field class=\"template_field\" name=\"what\">what</template_field>', NULL, 'edit', NULL, NULL, '%MODULES_TEST_HTTP%', 'edit_test.php', '<template_field class=\"template_field\" name=\"edit_test\">edit_test</template_field>', NULL, 0, 0, 5, '%MODULES_TEST%'),
(39, 'cancella <template_field class=\"template_field\" name=\"what\">what</template_field>', NULL, 'trash', NULL, NULL, '%MODULES_TEST_HTTP%', 'edit_test.php', '<template_field class=\"template_field\" name=\"delete_test\">delete_test</template_field>', NULL, 0, 0, 10, '%MODULES_TEST%'),
(40, 'aggiorna report', NULL, 'refresh', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=student&mode=update', 'id_instance, id_course', NULL, 0, 0, 5, '%ALWAYS%'),
(41, 'indice del corso', NULL, 'info letter', NULL, NULL, '%HTTP_ROOT_DIR%/browsing', 'main_index.php?order=struct&hide_visits=0&expand=10', 'id_course_instance,id_course', NULL, 0, 0, 10, '%ALWAYS%'),
(61, 'indice del forum', NULL, 'basic community', NULL, NULL, '%HTTP_ROOT_DIR%/browsing', 'main_index.php?op=forum&order=struct&hide_visits=0&expand=10', 'id_course_instance,id_course', NULL, 0, 0, 15, '%ALWAYS%'),
(42, 'chiudi', NULL, 'off', 'large', '{\"onclick\":\"closeMeAndReloadParent();\"}', NULL, NULL, NULL, NULL, 1, 0, 500, '%ALWAYS%'),
(43, 'scrivi', NULL, 'pencil', 'large', '', '%HTTP_ROOT_DIR%/comunica', 'send_message.php', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(44, 'messaggi inviati', NULL, 'forward mail', 'large', NULL, '%HTTP_ROOT_DIR%/comunica', 'list_messages.php?messages=sent', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(45, 'messaggi', NULL, 'mail', 'large', NULL, '%HTTP_ROOT_DIR%/comunica', 'list_messages.php', NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(46, 'crea appuntamento', NULL, 'opne folder', 'large', NULL, ' %HTTP_ROOT_DIR%/comunica', 'send_event.php', '', NULL, 0, 0, 0, '{\r\n\"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}\r\n'),
(47, 'appuntamenti', NULL, 'open folder', 'large', NULL, ' %HTTP_ROOT_DIR%/comunica', 'list_events.php', NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(48, 'chiudi', NULL, 'off', 'large', '{\"onclick\":\"exitChat();\"}', NULL, NULL, NULL, NULL, 1, 0, 500, '%ALWAYS%'),
(49, 'Messaggeria\r\n', NULL, 'mail', NULL, '{\"onclick\":\"openMessenger(\'..//comunica/list_messages.php\',800,600);\"}', NULL, NULL, NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(50, 'chatrooms', NULL, 'chat', NULL, NULL, ' %HTTP_ROOT_DIR%/comunica', 'list_chatrooms.php', NULL, NULL, 0, 0, 10, '%ALWAYS%'),
(51, 'agenda', NULL, 'calendar', NULL, '{\"onclick\":\"openMessenger(\'..//comunica/list_events.php\',800,600);\"}', NULL, NULL, NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(52, 'messaggi ricevuti', NULL, 'reply mail', 'large', NULL, '%HTTP_ROOT_DIR%/comunica', 'list_messages.php?messages=received', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(60, 'crea chatroom', NULL, 'chat', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'create_chat.php', NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(54, 'chiudi', NULL, 'off', 'large', '{\"onclick\":\"<template_field class=\\\"template_field\\\" name=\\\"onclick\\\">onclick</template_field>\"}', NULL, NULL, NULL, NULL, 1, 0, 500, '%ALWAYS%'),
(55, 'annulla', NULL, 'remove', 'large', '{\"onclick\":\"<template_field class=\\\"template_field\\\" name=\\\"cancel\\\">cancel</template_field>\"} ', NULL, NULL, NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(56, 'indietro', NULL, 'circle left', 'large', '{\"onclick\": \"javascript:history.go(-1);\"}', NULL, NULL, NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(57, 'modifica', NULL, 'edit', 'large', NULL, '%HTTP_ROOT_DIR%/services', '<template_field class=\"template_field\" name=\"edit_link\">edit_link</template_field>', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(58, 'salva', '', 'archive', 'large', NULL, '%HTTP_ROOT_DIR%/services', '<template_field class=\"template_field\" name=\"save_link\">save_link</template_field>', '', NULL, 0, 0, 10, '%ALWAYS%'),
(101, 'lista autori', '', 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/switcher', 'list_users.php?list=authors', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(62, 'report della chat', NULL, 'basic docs', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'report_chat.php?op=index', 'id_instance,id_course', NULL, 0, 0, 20, '%ALWAYS%'),
(63, 'lista chatrooms', NULL, 'chat outline', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'list_chatrooms.php', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(64, 'esporta report (XLS)', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=export&type=xls', 'id_instance,id_course', NULL, 0, 0, 25, '%ALWAYS%'),
(65, 'esporta report (PDF)', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=export&type=pdf', 'id_instance,id_course', NULL, 0, 0, 30, '%ALWAYS%'),
(66, 'esporta testo', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'report_chat.php?op=export', 'id_chatroom', NULL, 0, 0, 20, '%ALWAYS%'),
(67, 'esporta foglio', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'report_chat.php?op=exportTable', 'id_chatroom', NULL, 0, 0, 25, '%ALWAYS%'),
(68, 'elenco studenti', NULL, 'list', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=student', 'id_instance', NULL, 0, 0, 5, '%ALWAYS%'),
(69, 'cronologia completa', NULL, 'basic content', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history.php', 'id_course_instance,id_student,id_course', NULL, 0, 0, 15, '%ALWAYS%'),
(70, 'scheda corsista', NULL, 'building', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=zoom_student', 'id_student,id_instance', NULL, 0, 0, 0, '%ALWAYS%'),
(71, 'punteggio', NULL, 'flag', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history.php?mode=score', 'id_student,id_course_instance', NULL, 0, 0, 5, '%ALWAYS%'),
(72, 'interazione', NULL, 'users', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history.php?mode=writings', 'id_student,id_course_instance', NULL, 0, 0, 15, '%ALWAYS%'),
(73, 'indice attività', NULL, 'info letter', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history.php?mode=summary', 'id_student,id_course_instance', NULL, 0, 0, 30, '%ALWAYS%'),
(74, 'esporta report corsista', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history.php?op=export', 'id_student,id_course_instance', NULL, 0, 0, 35, '%ALWAYS%'),
(75, 'Nodi visitati recentemente', '<label>Nodi visitati recentemente :</label>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 40, '%ALWAYS%'),
(76, '1 giorno', NULL, 'circle', 'small', NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history_details.php?period=1', 'id_student,id_course_instance,id_course', NULL, 0, 0, 45, '%ALWAYS%'),
(77, '5 giorni', NULL, 'circle', 'small', NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history_details.php?period=5', 'id_student,id_course_instance,id_course', NULL, 0, 0, 50, '%ALWAYS%'),
(78, '15 giorni', NULL, 'circle', 'small', NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history_details.php?period=15', 'id_student,id_course_instance,id_course', NULL, 0, 0, 55, '%ALWAYS%'),
(79, '30 giorni', NULL, 'circle', 'small', NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history_details.php?period=30', 'id_student,id_course_instance,id_course', NULL, 0, 0, 60, '%ALWAYS%'),
(80, 'tutto', NULL, 'circle', 'small', NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history_details.php?period=all', 'id_student,id_course_instance,id_course', NULL, 0, 0, 65, '%ALWAYS%'),
(81, 'esercizi svolti', NULL, 'attachment', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_exercise.php', 'id_student,id_course_instance', NULL, 0, 0, 20, '%ALWAYS%'),
(82, 'note', NULL, 'pencil', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=student_notes', 'id_student,id_instance', NULL, 0, 0, 20, '%ALWAYS%'),
(83, 'esporta note corsista', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=student_notes_export', 'id_student,id_instance', NULL, 0, 0, 40, '%ALWAYS%'),
(84, 'esporta report corsista (XLS)', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history_details.php?op=export', 'id_student,id_course_instance,id_course,period', NULL, 0, 0, 10, '%ALWAYS%'),
(85, 'esporta report corsista (PDF)', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history_details.php?op=export&type=pdf', 'id_student,id_course_instance,id_course,period', NULL, 0, 0, 15, '%ALWAYS%'),
(86, 'riepilogo cronologia', NULL, 'basic book', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor_history.php', 'id_course_instance,id_student', NULL, 0, 0, 40, '%ALWAYS%'),
(100, 'aggiungi sondaggio', NULL, 'basic chart', NULL, NULL, '%MODULES_TEST_HTTP%', 'edit_test.php?mode=survey&action=add', 'id_node', NULL, 0, 0, 25, '%MODULES_TEST_MOD_ENABLED%'),
(99, 'aggiungi test', NULL, 'question', NULL, NULL, '%MODULES_TEST_HTTP%', 'edit_test.php?mode=test&action=add', 'id_node', NULL, 0, 0, 20, '%MODULES_TEST_MOD_ENABLED%'),
(97, 'aggiungi termine', NULL, 'basic asterisk', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'addnode.php?type=WORD', 'id_parent,id_course', NULL, 0, 0, 10, '$mod_enabled'),
(98, 'aggiungi esercizio', NULL, 'basic edit', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'add_exercise.php?', 'id_node', NULL, 0, 0, 10, '$mod_enabled'),
(95, 'report', NULL, 'basic docs', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'author_report.php', NULL, NULL, 0, 0, 0, '%ALWAYS%'),
(96, 'aggiungi nodo', NULL, 'basic doc', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'addnode.php?type=LEAF', 'id_parent,id_course', NULL, 0, 0, 0, '$mod_enabled'),
(113, 'torna', '\r\n<a id=\"torna\" href=\"translation.php\" class=\"item\"><i class=\"circle left icon large\"></i>\r\n<span  class=\"menulabel\">Indietro</span>\r\n</a>\r\n \r\n\r\n', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '%ALWAYS%'),
(103, 'lista studenti', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/switcher', 'list_users.php?list=students', NULL, NULL, 0, 0, 15, '%ALWAYS%'),
(104, 'aggiungi utente', NULL, 'basic add user', NULL, NULL, '%HTTP_ROOT_DIR%/switcher', 'add_user.php', NULL, NULL, 0, 0, 20, '%ALWAYS%'),
(105, 'lista corsi', NULL, 'browser', NULL, NULL, '%HTTP_ROOT_DIR%/switcher', 'list_courses.php', NULL, NULL, 0, 0, 25, '%ALWAYS%'),
(106, 'aggiungi corso', NULL, 'book', NULL, NULL, '%HTTP_ROOT_DIR%/switcher', 'add_course.php', NULL, NULL, 0, 0, 30, '%ALWAYS%'),
(107, 'traduci messaggi', NULL, 'edit', NULL, NULL, '%HTTP_ROOT_DIR%/switcher', 'translation.php', NULL, NULL, 0, 0, 30, '%ALWAYS%'),
(108, 'applicazioni', NULL, 'settings', NULL, NULL, '%MODULES_APPS_HTTP%', '', NULL, NULL, 0, 0, 40, '%MODULES_APPS%'),
(111, 'condizioni di completamento', NULL, 'basic pin', NULL, NULL, '%MODULES_SERVICECOMPLETE_HTTP%', 'index.php', NULL, NULL, 0, 0, 45, '%MODULES_SERVICECOMPLETE%'),
(112, 'newsletter', NULL, 'mail outline', '', NULL, '%MODULES_NEWSLETTER_HTTP%', NULL, NULL, NULL, 0, 0, 15, '%MODULES_NEWSLETTER%'),
(114, 'aggiungi provider', NULL, 'archive', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'add_tester.php', NULL, NULL, 0, 0, 0, '%NEVER%'),
(115, 'aggiungi servizio', NULL, 'tags', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'add_service.php', NULL, NULL, 0, 0, 0, '%NEVER%'),
(116, 'aggiungi utente', NULL, 'basic add user', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'add_user.php', NULL, NULL, 0, 0, 10, '%ALWAYS%'),
(117, 'importa una lingua', NULL, 'download disk', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'import_language.php', NULL, NULL, 0, 0, 15, '%NEVER%\r\n'),
(118, 'edit news content', NULL, 'edit', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'edit_content.php?type=news', NULL, NULL, 0, 0, 25, '%ALWAYS%'),
(119, 'edit info content', NULL, 'edit', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'edit_content.php?type=info', NULL, NULL, 0, 0, 30, '%ALWAYS%'),
(120, 'edit help content', NULL, 'edit', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'edit_content.php?type=help', NULL, NULL, 0, 0, 30, '%ALWAYS%'),
(121, 'modifica il profilo del provider', NULL, 'pencil', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'edit_tester.php', 'id_tester', NULL, 0, 0, 0, '%ALWAYS%'),
(122, 'gestisci associazione corsi', NULL, 'unlink', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'manage_provider_courses.php', 'id_tester', NULL, 0, 0, 5, '%ALWAYS%'),
(123, 'lista utenti', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'list_users.php', 'id_tester', NULL, 0, 0, 15, '%ALWAYS%'),
(124, 'admins list', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'list_users.php?user_type=2', 'id_tester', NULL, 0, 0, 0, '%ALWAYS%'),
(125, 'switcher list', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'list_users.php?user_type=6', 'id_tester', NULL, 0, 0, 10, '%ALWAYS%'),
(126, 'authors list', NULL, 'basic users', NULL, '', '%HTTP_ROOT_DIR%/admin', 'list_users.php?user_type=1', 'id_tester', NULL, 0, 0, 15, '%ALWAYS%'),
(127, 'tutors list', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'list_users.php?user_type=4', 'id_tester', NULL, 0, 0, 20, '%ALWAYS%'),
(128, 'students list', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'list_users.php?user_type=3', 'id_tester', NULL, 0, 0, 25, '%ALWAYS%'),
(129, 'all users list', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'list_users.php', 'id_tester', NULL, 0, 0, 30, '%ALWAYS%'),
(130, 'rispondi', NULL, 'forward mail', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'send_message.php?op=replay', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(131, 'rispondi a tutti', NULL, 'forward mail', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'send_message.php?op=replay_all', NULL, NULL, 0, 0, 10, '{\r\n\"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}'),
(132, 'cancella', NULL, 'trash', NULL, NULL, '%HTTP_ROOT_DIR%/comunica', 'read_message.php', 'del_msg_id', NULL, 0, 0, 20, '{\r\n\"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}'),
(133, 'modifica domanda', NULL, 'edit', NULL, NULL, '%MODULES_TEST_HTTP%', 'edit_question.php', '<template_field class=\"template_field\" name=\"edit_question\">edit_question</template_field>', NULL, 0, 0, 0, '%MODULES_TEST%'),
(134, 'cancella domanda', NULL, 'trash', NULL, NULL, '%MODULES_TEST_HTTP%', 'edit_question.php', '<template_field class=\"template_field\" name=\"delete_question\">delete_question</template_field>', NULL, 0, 0, 0, '%MODULES_TEST%'),
(135, 'elimina esercizio', NULL, 'trash', NULL, '{\"onclick\":\"<template_field class=\\\"template_field\\\" name=\\\"onclick\\\">onclick</template_field>\"}', NULL, NULL, NULL, NULL, 0, 0, 10, '%ALWAYS%'),
(147, 'comunità di tutor', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'communities.php', NULL, NULL, 0, 0, 10, '%NOT_SUPERTUTOR%'),
(136, 'Modifica esercizio', NULL, 'edit', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'edit_exercise.php?op=edit', NULL, NULL, 0, 0, 5, '%ALWAYS%'),
(137, 'Edit home page contents', NULL, 'edit', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'edit_content.php', NULL, NULL, 0, 0, 40, '%NON_MULTIPROVIDER_MENU%'),
(138, 'Gestione luoghi', NULL, 'building', NULL, NULL, '%MODULES_CLASSROOM_HTTP%', 'venues.php', NULL, NULL, 0, 0, 20, '%MODULES_CLASSROOM%'),
(139, 'Gestione Aule', NULL, 'grid layout', NULL, NULL, '%MODULES_CLASSROOM_HTTP%', 'classrooms.php\r\n\r\n', NULL, NULL, 0, 0, 20, '%MODULES_CLASSROOM%'),
(140, 'Calendario', NULL, 'calendar outline', NULL, NULL, '%MODULES_CLASSAGENDA_HTTP%', 'calendars.php', NULL, NULL, 0, 0, 30, '%MODULES_CLASSAGENDA%'),
(141, 'report provider', NULL, 'basic docs', NULL, NULL, '%HTTP_ROOT_DIR%/admin', 'log_report.php', NULL, NULL, 0, 0, 30, '%ALWAYS%'),
(142, 'esporta', NULL, 'basic export', 'large', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '{\r\n"func\": [\"\\\\ADAGenericUser\",\"isNotStudent\"]\r\n}'),
(143, 'File PDF', NULL, 'file outline', '', '{\"data-type\":\"pdf\"}', '%MODULES_CLASSAGENDA_HTTP%', 'exportCalendar.php?type=pdf', NULL, 'calendarexportmenuitem', 0, 0, 0, '%MODULES_CLASSAGENDA%'),
(144, 'File CSV', NULL, 'file', '', '{\"data-type\":\"csv\"}', '%MODULES_CLASSAGENDA_HTTP%', 'exportCalendar.php?type=csv', NULL, 'calendarexportmenuitem', 0, 0, 0, '%MODULES_CLASSAGENDA%'),
(145, 'File PDF', NULL, 'file outline', NULL, '{\"target\":\"_blank\"}', '%MODULES_CLASSBUDGET_HTTP%', 'index.php?export=pdf', 'id_course, id_course_instance', NULL, 0, 0, 0, '%MODULES_CLASSBUDGET%'),
(146, 'File CSV', NULL, 'file', NULL, NULL, '%MODULES_CLASSBUDGET_HTTP%', 'index.php?export=csv', 'id_course, id_course_instance', NULL, 0, 0, 0, '%MODULES_CLASSBUDGET%'),
(148, 'lista tutor', NULL, 'basic users', NULL, NULL, '%HTTP_ROOT_DIR%/tutor', 'tutor.php?op=tutor', NULL, NULL, 0, 0, 10, '%IS_SUPERTUTOR%'),
(149, 'Aggiungi sessione di attività', NULL, 'question', NULL, NULL, '%MODULES_TEST_HTTP%', 'edit_test.php?mode=activity&action=add', 'id_node', NULL, 0, 0, 18, '%MODULES_TEST%'),
(150, 'Impostazioni Login', NULL, 'exchange basic', NULL, NULL, '%MODULES_LOGIN_HTTP%', '', NULL, NULL, 0, 0, 20, '%MODULES_LOGIN%'),
(152, 'Assistenza', NULL, 'mail', NULL, NULL, '%MODULES_FORMMAIL_HTTP%', NULL, NULL, NULL, 0, 0, 0, '{\"func\":\"menuEnableFormMail\"}'),
(163, 'Aggiungi nota di classe', NULL, 'docs basic', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'addnode.php?type=NOTE', 'id_parent,id_course,id_course_instance', NULL, 0, 0, 0, '$mod_enabled'),
(164, 'Aggiungi nota personale', NULL, 'doc basic', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'addnode.php?type=PRIVATE_NOTE', 'id_parent,id_course,id_course_instance', NULL, 0, 0, 0, '$mod_enabled'),
(165, 'Modifica nota', NULL, 'pencil', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'edit_node.php?op=edit', 'id_parent,id_course,id_course_instance,type', NULL, 0, 0, 0, '{\"func\": [\"\\\\BrowsingHelper\",\"isSessionUserAuthorOfSessionNote\"]}'),
(166, 'Elimina nota', NULL, 'trash', NULL, NULL, '%HTTP_ROOT_DIR%/services', 'edit_node.php?op=delete', 'id_parent,id_course,id_course_instance,type', NULL, 0, 0, 0, '{\"func\": [\"\\\\BrowsingHelper\",\"isSessionUserAuthorOfSessionNote\"]}'),
(167, 'Badges', NULL, 'certificate', NULL, NULL, '%MODULES_BADGES_HTTP%', NULL, NULL, NULL, 0, 0, 100, '%MODULES_BADGES%'),
(168, 'Badges', NULL, 'certificate', NULL, NULL, '%MODULES_BADGES_HTTP%', 'user-badges.php', NULL, NULL, 0, 0, 120, '%MODULES_BADGES%'),
(169, 'Privacy', NULL, 'id basic', 'large', NULL, NULL, NULL, NULL, NULL, 1, 0, 5, '%MODULES_GDPR%'),
(170, 'Nuova richiesta', NULL, 'file outline', NULL, NULL, '%MODULES_GDPR_HTTP%', NULL, NULL, NULL, 0, 0, 5, '%MODULES_GDPR%'),
(171, 'Le mie richieste', NULL, 'tasks', NULL, NULL, '%MODULES_GDPR_HTTP%', 'list.php', NULL, NULL, 0, 0, 10, '%MODULES_GDPR%'),
(172, 'Tutte le richieste', NULL, 'text file outline', NULL, NULL, '%MODULES_GDPR_HTTP%', 'list.php?showall=1', NULL, NULL, 0, 0, 15, '{\"func\": [\"\\\\Lynxlab\\\\ADA\\\\Module\\\\GDPR\\\\GdprActions\",\"canDo\"],\"params\":{ \"value1\": {\"func\": [\"\\\\Lynxlab\\\\ADA\\\\Module\\\\GDPR\\\\GdprActions\",\"getConstantFromString\"],\"params\": \"ACCESS_ALL_REQUESTS\"}}}'),
(173, 'Gestione politiche', NULL, 'legal', NULL, NULL, '%MODULES_GDPR_HTTP%', 'listPolicies.php', NULL, NULL, 0, 0, 15, '{\"func\": [\"\\\\Lynxlab\\\\ADA\\\\Module\\\\GDPR\\\\GdprActions\",\"canDo\"],\"params\":{ \"value1\": {\"func\": [\"\\\\Lynxlab\\\\ADA\\\\Module\\\\GDPR\\\\GdprActions\",\"getConstantFromString\"],\"params\": \"LIST_POLICIES\"}}}'),
(174, 'Cerca pratica', NULL, 'basic search', NULL, NULL, '%MODULES_GDPR_HTTP%', 'lookuprequest.php', NULL, NULL, 0, 0, 20, '%ALWAYS%'),
(175, 'Accettazione politiche', NULL, 'checkmark sign', NULL, NULL, '%MODULES_GDPR_HTTP%', 'acceptPolicies.php', NULL, NULL, 0, 0, 25, '%MODULES_GDPR%'),
(176, 'importa corso', NULL, 'download disk', NULL, NULL, '%MODULES_IMPEXPORT_HTTP%', 'import.php', NULL, NULL, 0, 0, 45, '%MODULES_IMPEXPORT%'),
(177, 'esporta corso', NULL, 'upload disk', NULL, NULL, '%MODULES_IMPEXPORT_HTTP%', 'export.php', NULL, NULL, 0, 0, 50, '%MODULES_IMPEXPORT%'),
(178, 'repository corsi', NULL, 'basic book', NULL, NULL, '%MODULES_IMPEXPORT_HTTP%', 'repository.php', NULL, NULL, 0, 0, 55, '%MODULES_IMPEXPORT%'),
(12, 'Naviga', NULL, 'globe', 'large', NULL, '%HTTP_ROOT_DIR%/browsing', 'map.php', 'id_node', NULL, 0, 0, 25, '%ALWAYS%');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`item_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Mag 14, 2020 alle 20:15
-- Versione del server: 10.3.15-MariaDB
-- Versione PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `commonDB`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `menu_tree`
--

CREATE TABLE `menu_tree` (
  `tree_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `item_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `extraClass` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `menu_tree`
--

INSERT INTO `menu_tree` (`tree_id`, `parent_id`, `item_id`, `extraClass`) VALUES
(1, 0, 1, 'homeExtraClass'),
(1, 0, 2, 'comunicaExtraClass'),
(1, 2, 3, ''),
(1, 2, 4, ''),
(1, 0, 9, ''),
(1, 0, 10, ''),
(1, 0, 140, ''),
(2, 0, 14, ''),
(2, 0, 15, ''),
(2, 0, 16, ''),
(2, 16, 17, ''),
(2, 16, 18, ''),
(3, 0, 1, ''),
(3, 0, 15, ''),
(3, 0, 16, ''),
(3, 16, 17, ''),
(3, 16, 18, ''),
(4, 0, 1, ''),
(4, 0, 14, ''),
(4, 0, 16, ''),
(4, 16, 17, ''),
(4, 16, 18, ''),
(5, 0, 1, ''),
(5, 0, 2, ''),
(5, 2, 3, ''),
(5, 0, 16, ''),
(5, 16, 17, ''),
(5, 16, 18, ''),
(5, 0, 9, ''),
(5, 0, 20, ''),
(5, 20, 21, ''),
(5, 0, 2200, ''),
(5, 22, 23, ''),
(5, 0, 15, ''),
(5, 0, 24, ''),
(1, 0, 24, ''),
(1, 2, 25, ''),
(1, 2, 26, ''),
(1, 2, 27, ''),
(1, 20, 28, ''),
(1, 20, 29, ''),
(1, 20, 30, ''),
(1, 20, 31, ''),
(1, 20, 32, ''),
(1, 20, 33, ''),
(1, 0, 20, ''),
(1, 20, 21, ''),
(1, 0, 22, ''),
(1, 22, 23, ''),
(1, 22, 34, ''),
(79, 20, 21, ''),
(79, 2, 3, ''),
(1, 0, 16, ''),
(1, 16, 17, ''),
(1, 16, 18, ''),
(6, 0, 1, ''),
(6, 0, 20, ''),
(6, 20, 28, ''),
(6, 20, 29, ''),
(6, 20, 31, ''),
(6, 20, 32, ''),
(6, 20, 33, ''),
(6, 0, 22, ''),
(6, 22, 23, ''),
(6, 0, 16, ''),
(6, 16, 17, ''),
(6, 16, 18, ''),
(6, 0, 9, ''),
(6, 0, 24, ''),
(6, 0, 10, ''),
(7, 0, 37, ''),
(7, 0, 22, ''),
(7, 22, 38, ''),
(7, 22, 39, ''),
(7, 0, 24, ''),
(7, 0, 9, ''),
(9, 22, 100, ''),
(9, 0, 11, ''),
(10, 0, 1, ''),
(10, 0, 2, ''),
(10, 2, 3, ''),
(10, 0, 20, ''),
(10, 20, 21, ''),
(10, 0, 15, ''),
(10, 0, 16, ''),
(10, 16, 17, ''),
(10, 16, 18, ''),
(10, 0, 9, ''),
(10, 0, 24, ''),
(10, 0, 22, ''),
(10, 22, 23, ''),
(11, 0, 1, ''),
(11, 0, 2, ''),
(11, 0, 9, ''),
(11, 0, 15, ''),
(11, 0, 16, ''),
(11, 0, 20, ''),
(11, 0, 24, ''),
(11, 2, 3, ''),
(11, 16, 17, ''),
(11, 16, 18, ''),
(11, 20, 21, ''),
(12, 0, 1, ''),
(12, 0, 22, ''),
(12, 22, 23, ''),
(12, 22, 40, ''),
(12, 22, 41, ''),
(13, 0, 42, ''),
(13, 0, 43, ''),
(13, 0, 44, ''),
(13, 0, 24, ''),
(14, 0, 42, ''),
(14, 0, 45, ''),
(14, 0, 24, ''),
(16, 0, 42, ''),
(16, 0, 24, ''),
(16, 0, 46, ''),
(17, 0, 42, ''),
(17, 0, 24, ''),
(17, 0, 47, ''),
(108, 0, 1, ''),
(106, 0, 113, ''),
(106, 16, 18, ''),
(106, 16, 17, ''),
(22, 0, 16, ''),
(22, 0, 1, ''),
(20, 0, 1, ''),
(20, 0, 2, ''),
(20, 2, 49, ''),
(20, 2, 50, ''),
(20, 0, 20, ''),
(20, 0, 22, ''),
(20, 20, 51, ''),
(20, 22, 23, ''),
(20, 0, 24, ''),
(20, 0, 16, ''),
(20, 0, 9, ''),
(20, 16, 17, ''),
(20, 16, 18, ''),
(21, 0, 42, ''),
(21, 0, 43, ''),
(21, 0, 52, ''),
(21, 0, 24, ''),
(22, 0, 9, ''),
(22, 16, 17, ''),
(22, 16, 18, ''),
(22, 0, 10, ''),
(22, 0, 24, ''),
(23, 0, 48, ''),
(24, 0, 54, ''),
(24, 0, 24, ''),
(9, 22, 99, ''),
(82, 0, 24, ''),
(82, 0, 55, ''),
(84, 0, 56, ''),
(84, 0, 57, ''),
(84, 0, 58, ''),
(28, 0, 24, ''),
(28, 0, 1, ''),
(28, 0, 15, ''),
(28, 0, 16, ''),
(28, 16, 17, ''),
(28, 16, 18, ''),
(28, 0, 9, ''),
(29, 0, 42, ''),
(29, 0, 24, ''),
(30, 0, 1, ''),
(30, 0, 22, ''),
(30, 22, 23, ''),
(30, 0, 15, ''),
(30, 0, 16, ''),
(30, 16, 17, ''),
(30, 16, 18, ''),
(30, 0, 24, ''),
(30, 0, 9, ''),
(6, 0, 11, ''),
(34, 0, 1, ''),
(34, 0, 2, ''),
(34, 0, 20, ''),
(34, 2, 3, ''),
(34, 2, 50, ''),
(40, 0, 1, ''),
(40, 0, 2, ''),
(40, 2, 3, ''),
(40, 0, 20, ''),
(40, 20, 21, ''),
(40, 0, 22, ''),
(40, 22, 60, ''),
(40, 0, 24, ''),
(40, 0, 16, ''),
(40, 16, 17, ''),
(40, 16, 18, ''),
(40, 0, 9, ''),
(34, 20, 21, ''),
(34, 0, 22, ''),
(34, 22, 23, ''),
(34, 0, 15, ''),
(34, 0, 24, ''),
(34, 0, 16, ''),
(34, 16, 17, ''),
(34, 16, 18, ''),
(34, 0, 9, ''),
(45, 0, 2, ''),
(45, 2, 3, ''),
(45, 2, 4, ''),
(45, 2, 25, ''),
(45, 2, 26, ''),
(45, 2, 27, ''),
(45, 0, 16, ''),
(45, 16, 17, ''),
(45, 16, 18, ''),
(45, 0, 1, ''),
(45, 0, 9, ''),
(45, 0, 10, ''),
(45, 0, 11, ''),
(45, 0, 20, ''),
(45, 0, 22, ''),
(45, 0, 24, ''),
(45, 20, 21, ''),
(45, 20, 28, ''),
(45, 20, 29, ''),
(53, 2, 3, ''),
(53, 0, 2, ''),
(53, 0, 1, ''),
(45, 20, 33, ''),
(45, 22, 23, ''),
(45, 22, 34, ''),
(79, 2, 63, ''),
(84, 0, 24, ''),
(53, 2, 50, ''),
(53, 0, 20, ''),
(53, 20, 21, ''),
(53, 0, 22, ''),
(53, 22, 23, ''),
(53, 22, 40, ''),
(53, 22, 41, ''),
(53, 22, 61, ''),
(53, 22, 62, ''),
(56, 0, 1, ''),
(56, 0, 2, ''),
(56, 0, 24, ''),
(56, 0, 9, ''),
(56, 0, 16, ''),
(56, 16, 17, ''),
(56, 16, 18, ''),
(56, 2, 3, ''),
(56, 2, 63, ''),
(56, 0, 20, ''),
(56, 20, 21, ''),
(56, 0, 22, ''),
(56, 22, 25, ''),
(53, 22, 64, ''),
(53, 22, 65, ''),
(53, 0, 24, ''),
(53, 0, 9, ''),
(53, 0, 16, ''),
(53, 16, 17, ''),
(53, 16, 18, ''),
(41, 0, 1, ''),
(41, 0, 2, ''),
(41, 2, 3, ''),
(42, 0, 63, ''),
(41, 2, 63, ''),
(41, 0, 20, ''),
(41, 20, 21, ''),
(41, 0, 24, ''),
(41, 0, 9, ''),
(41, 0, 16, ''),
(41, 16, 17, ''),
(41, 16, 18, ''),
(57, 0, 1, ''),
(57, 0, 2, ''),
(57, 0, 9, ''),
(57, 0, 16, ''),
(57, 0, 20, ''),
(57, 0, 22, ''),
(57, 0, 24, ''),
(57, 2, 3, ''),
(57, 16, 17, ''),
(57, 16, 18, ''),
(57, 20, 21, ''),
(57, 22, 60, ''),
(57, 2, 63, ''),
(58, 0, 1, ''),
(58, 0, 2, ''),
(58, 0, 9, ''),
(58, 0, 16, ''),
(58, 0, 20, ''),
(58, 0, 22, ''),
(58, 0, 24, ''),
(58, 2, 3, ''),
(58, 2, 63, ''),
(58, 16, 17, ''),
(58, 16, 18, ''),
(58, 20, 21, ''),
(58, 22, 25, ''),
(58, 22, 66, ''),
(58, 22, 67, ''),
(59, 0, 1, ''),
(59, 0, 2, ''),
(59, 2, 50, ''),
(59, 2, 3, ''),
(59, 0, 20, ''),
(59, 20, 21, ''),
(59, 0, 22, ''),
(59, 22, 23, ''),
(59, 22, 68, ''),
(59, 22, 69, ''),
(60, 0, 1, ''),
(60, 0, 2, ''),
(60, 2, 3, ''),
(60, 2, 4, ''),
(60, 0, 20, ''),
(60, 20, 21, ''),
(60, 0, 22, ''),
(60, 22, 70, ''),
(60, 22, 71, ''),
(60, 22, 72, ''),
(60, 22, 73, ''),
(60, 22, 74, ''),
(60, 22, 68, ''),
(60, 22, 75, ''),
(60, 22, 76, ''),
(60, 22, 77, ''),
(60, 22, 78, ''),
(60, 22, 80, ''),
(60, 0, 24, ''),
(60, 0, 9, ''),
(60, 0, 16, ''),
(60, 16, 17, ''),
(60, 16, 18, ''),
(59, 22, 81, ''),
(59, 22, 82, ''),
(59, 0, 24, ''),
(59, 0, 9, ''),
(59, 0, 16, ''),
(59, 16, 17, ''),
(59, 16, 18, ''),
(61, 0, 1, ''),
(61, 0, 2, ''),
(61, 2, 3, ''),
(61, 0, 20, ''),
(61, 20, 21, ''),
(61, 0, 22, ''),
(61, 22, 70, ''),
(61, 22, 68, ''),
(61, 0, 24, ''),
(61, 0, 9, ''),
(61, 0, 16, ''),
(61, 16, 17, ''),
(61, 16, 18, ''),
(62, 0, 1, ''),
(62, 0, 2, ''),
(62, 2, 3, ''),
(62, 2, 50, ''),
(62, 0, 20, ''),
(62, 20, 21, ''),
(62, 0, 22, ''),
(62, 22, 23, ''),
(62, 22, 68, ''),
(62, 22, 69, ''),
(62, 22, 81, ''),
(62, 22, 70, ''),
(62, 22, 83, ''),
(62, 0, 24, ''),
(62, 0, 9, ''),
(62, 0, 16, ''),
(62, 16, 17, ''),
(62, 16, 18, ''),
(63, 0, 1, ''),
(63, 0, 2, ''),
(63, 2, 3, ''),
(63, 0, 20, ''),
(63, 20, 21, ''),
(63, 0, 22, ''),
(63, 22, 70, ''),
(63, 22, 84, ''),
(63, 22, 85, ''),
(63, 22, 68, ''),
(63, 22, 86, ''),
(63, 0, 24, ''),
(63, 0, 9, ''),
(63, 0, 16, ''),
(63, 16, 17, ''),
(63, 16, 18, ''),
(79, 22, 108, ''),
(79, 22, 107, ''),
(79, 22, 106, ''),
(79, 22, 105, ''),
(79, 22, 104, ''),
(79, 22, 103, ''),
(79, 22, 102, ''),
(79, 22, 101, ''),
(79, 22, 23, ''),
(79, 22, 35, ''),
(79, 0, 20, ''),
(79, 16, 18, ''),
(79, 16, 17, ''),
(79, 0, 16, ''),
(79, 0, 9, ''),
(79, 0, 24, ''),
(79, 0, 22, ''),
(9, 16, 18, ''),
(9, 16, 17, ''),
(9, 0, 16, ''),
(9, 0, 9, ''),
(9, 0, 24, ''),
(77, 0, 24, ''),
(77, 0, 56, ''),
(77, 0, 9, ''),
(9, 22, 98, ''),
(9, 22, 97, ''),
(9, 22, 96, ''),
(9, 22, 23, ''),
(9, 0, 22, ''),
(9, 20, 33, ''),
(9, 20, 28, ''),
(9, 0, 20, ''),
(9, 0, 10, ''),
(9, 2, 4, ''),
(9, 0, 2, ''),
(9, 0, 1, ''),
(106, 0, 1, ''),
(106, 0, 9, ''),
(106, 0, 16, ''),
(79, 0, 1, ''),
(106, 0, 24, ''),
(69, 16, 18, ''),
(69, 16, 17, ''),
(69, 0, 16, ''),
(69, 0, 9, ''),
(69, 0, 24, ''),
(79, 0, 2, ''),
(69, 22, 23, ''),
(69, 22, 95, ''),
(69, 0, 22, ''),
(69, 0, 1, ''),
(68, 16, 18, ''),
(68, 16, 17, ''),
(68, 0, 16, ''),
(68, 0, 9, ''),
(68, 0, 24, ''),
(68, 20, 51, ''),
(68, 0, 20, ''),
(68, 2, 50, ''),
(68, 2, 49, ''),
(68, 0, 2, ''),
(68, 0, 1, ''),
(1, 0, 12, ''),
(79, 22, 178, ''),
(79, 22, 111, ''),
(79, 2, 112, ''),
(105, 0, 1, ''),
(105, 0, 9, ''),
(105, 0, 16, ''),
(105, 0, 24, ''),
(105, 16, 17, ''),
(105, 16, 18, ''),
(108, 0, 2, ''),
(108, 2, 3, ''),
(102, 2, 112, ''),
(108, 2, 112, ''),
(108, 0, 20, ''),
(108, 20, 21, ''),
(108, 0, 22, ''),
(108, 0, 9, ''),
(108, 0, 16, ''),
(108, 16, 17, ''),
(108, 16, 18, ''),
(108, 22, 101, ''),
(108, 22, 102, ''),
(108, 22, 103, ''),
(108, 22, 104, ''),
(108, 22, 105, ''),
(108, 22, 106, ''),
(108, 22, 107, ''),
(108, 22, 108, ''),
(79, 22, 177, ''),
(79, 22, 176, ''),
(108, 22, 111, ''),
(124, 0, 1, ''),
(124, 0, 9, ''),
(124, 0, 16, ''),
(124, 0, 24, ''),
(124, 16, 17, ''),
(124, 16, 18, ''),
(124, 0, 56, ''),
(130, 0, 1, ''),
(130, 0, 9, ''),
(130, 0, 16, ''),
(130, 16, 17, ''),
(130, 16, 18, ''),
(130, 0, 22, ''),
(130, 22, 114, ''),
(130, 22, 115, ''),
(130, 22, 116, ''),
(130, 22, 117, ''),
(130, 22, 118, ''),
(130, 22, 119, ''),
(130, 22, 120, ''),
(132, 0, 22, ''),
(132, 22, 121, ''),
(132, 0, 1, ''),
(132, 0, 9, ''),
(132, 0, 16, ''),
(132, 16, 17, ''),
(132, 16, 18, ''),
(79, 22, 141, ''),
(132, 22, 123, ''),
(133, 0, 1, ''),
(133, 0, 22, ''),
(133, 0, 9, ''),
(133, 0, 24, ''),
(133, 0, 16, ''),
(133, 16, 17, ''),
(133, 16, 18, ''),
(130, 0, 24, ''),
(132, 0, 24, ''),
(133, 22, 124, ''),
(133, 22, 125, ''),
(132, 0, 0, ''),
(133, 22, 126, ''),
(133, 22, 127, ''),
(133, 22, 128, ''),
(133, 22, 129, ''),
(135, 0, 1, ''),
(135, 0, 16, ''),
(135, 16, 18, ''),
(135, 16, 17, ''),
(135, 0, 9, ''),
(135, 0, 11, ''),
(135, 0, 10, ''),
(138, 0, 16, ''),
(138, 16, 17, ''),
(138, 16, 18, ''),
(138, 0, 15, ''),
(138, 0, 14, ''),
(139, 0, 45, ''),
(139, 0, 22, ''),
(139, 0, 42, ''),
(139, 22, 43, ''),
(139, 22, 130, ''),
(139, 22, 131, ''),
(139, 22, 132, ''),
(142, 0, 42, ''),
(142, 0, 47, ''),
(145, 0, 9, ''),
(145, 0, 22, ''),
(145, 0, 56, ''),
(145, 22, 133, ''),
(145, 22, 134, ''),
(146, 0, 9, ''),
(146, 0, 22, ''),
(146, 0, 56, ''),
(146, 22, 135, ''),
(146, 22, 136, ''),
(79, 22, 137, ''),
(148, 0, 1, ''),
(148, 0, 9, ''),
(148, 0, 16, ''),
(148, 16, 17, ''),
(148, 16, 18, ''),
(148, 0, 24, ''),
(148, 0, 22, ''),
(148, 0, 2, ''),
(148, 2, 3, ''),
(148, 2, 112, ''),
(148, 22, 118, ''),
(148, 22, 119, ''),
(148, 22, 120, ''),
(146, 0, 24, ''),
(139, 0, 24, ''),
(4, 0, 24, ''),
(4, 0, 9, ''),
(156, 0, 1, ''),
(156, 0, 24, ''),
(156, 0, 42, ''),
(142, 0, 24, ''),
(79, 20, 138, ''),
(79, 20, 139, ''),
(79, 20, 140, ''),
(130, 22, 141, ''),
(161, 0, 1, ''),
(161, 0, 9, ''),
(161, 0, 16, ''),
(161, 0, 24, ''),
(161, 0, 56, ''),
(161, 16, 17, ''),
(161, 16, 18, ''),
(161, 0, 142, ''),
(161, 142, 143, ''),
(161, 142, 144, ''),
(162, 0, 1, ''),
(162, 0, 9, ''),
(162, 0, 16, ''),
(162, 0, 24, ''),
(162, 0, 56, ''),
(162, 0, 142, ''),
(162, 16, 17, ''),
(162, 16, 18, ''),
(162, 142, 145, ''),
(162, 142, 146, ''),
(34, 22, 148, ''),
(34, 20, 147, ''),
(5, 20, 140, ''),
(34, 20, 140, ''),
(9, 22, 149, ''),
(79, 22, 150, ''),
(162, 16, 152, ''),
(161, 16, 152, ''),
(148, 16, 152, ''),
(138, 16, 152, ''),
(135, 16, 152, ''),
(133, 16, 152, ''),
(132, 16, 152, ''),
(130, 16, 152, ''),
(124, 16, 152, ''),
(108, 16, 152, ''),
(106, 16, 152, ''),
(105, 16, 152, ''),
(79, 16, 152, ''),
(69, 16, 152, ''),
(68, 16, 152, ''),
(63, 16, 152, ''),
(62, 16, 152, ''),
(61, 16, 152, ''),
(60, 16, 152, ''),
(59, 16, 152, ''),
(58, 16, 152, ''),
(57, 16, 152, ''),
(56, 16, 152, ''),
(53, 16, 152, ''),
(45, 16, 152, ''),
(41, 16, 152, ''),
(40, 16, 152, ''),
(34, 16, 152, ''),
(30, 16, 152, ''),
(28, 16, 152, ''),
(22, 16, 152, ''),
(20, 16, 152, ''),
(11, 16, 152, ''),
(10, 16, 152, ''),
(9, 16, 152, ''),
(6, 16, 152, ''),
(5, 16, 152, ''),
(4, 16, 152, ''),
(3, 16, 152, ''),
(2, 16, 152, ''),
(1, 16, 152, ''),
(1, 22, 163, ''),
(1, 22, 164, ''),
(1, 22, 165, ''),
(1, 22, 166, ''),
(45, 22, 163, ''),
(45, 22, 164, ''),
(45, 22, 165, ''),
(45, 22, 166, ''),
(79, 22, 167, ''),
(5, 20, 168, ''),
(170, 0, 1, ''),
(170, 0, 9, ''),
(170, 0, 16, ''),
(170, 0, 24, ''),
(170, 0, 111, ''),
(170, 16, 17, ''),
(170, 16, 18, ''),
(170, 16, 152, ''),
(69, 0, 169, ''),
(69, 169, 170, ''),
(69, 169, 171, ''),
(69, 169, 172, ''),
(69, 169, 173, ''),
(69, 169, 175, ''),
(1, 0, 169, ''),
(1, 169, 170, ''),
(1, 169, 171, ''),
(1, 169, 175, ''),
(6, 0, 169, ''),
(6, 169, 170, ''),
(6, 169, 171, ''),
(6, 169, 175, ''),
(34, 0, 169, ''),
(34, 169, 170, ''),
(34, 169, 171, ''),
(34, 169, 172, ''),
(34, 169, 173, ''),
(34, 169, 175, ''),
(138, 0, 169, ''),
(138, 169, 174, ''),
(79, 0, 169, ''),
(79, 169, 170, ''),
(79, 169, 171, ''),
(79, 169, 172, ''),
(79, 169, 173, ''),
(79, 169, 175, '');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `menu_tree`
--
ALTER TABLE `menu_tree`
  ADD PRIMARY KEY (`tree_id`,`parent_id`,`item_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Mag 14, 2020 alle 20:15
-- Versione del server: 10.3.15-MariaDB
-- Versione PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `commonDB`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `menu_page`
--

CREATE TABLE `menu_page` (
  `tree_id` int(11) NOT NULL COMMENT 'id of the menu tree for the given module, script, user_type and self instruction',
  `module` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `script` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(2) NOT NULL,
  `self_instruction` int(1) NOT NULL DEFAULT 0 COMMENT 'nonzero if course is in self instruction mode',
  `isVertical` int(1) NOT NULL DEFAULT 0 COMMENT 'nonzero if it''s a vertical menu',
  `linked_tree_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `menu_page`
--

INSERT INTO `menu_page` (`tree_id`, `module`, `script`, `user_type`, `self_instruction`, `isVertical`, `linked_tree_id`) VALUES
(1, 'browsing', 'view.php', 3, 0, 0, NULL),
(2, 'main', 'default', 5, 0, 0, 105),
(3, 'browsing', 'default', 5, 0, 0, NULL),
(4, 'main', 'info.php', 5, 0, 0, NULL),
(5, 'browsing', 'user.php', 3, 0, 0, NULL),
(6, 'browsing', 'view.php', 3, 1, 0, NULL),
(7, 'modules/test', 'index.php', 1, 0, 0, NULL),
(8, 'modules/test', 'index.php', 3, 0, 0, 77),
(9, 'browsing', 'view.php', 1, 0, 0, NULL),
(10, 'browsing', 'default', 3, 0, 0, NULL),
(11, 'browsing', 'edit_user.php', 3, 0, 0, NULL),
(35, 'comunica', 'default', 4, 0, 0, 13),
(13, 'comunica', 'default', 3, 0, 0, NULL),
(14, 'comunica', 'send_message.php', 3, 0, 0, NULL),
(16, 'comunica', 'list_events.php', 3, 0, 0, NULL),
(17, 'comunica', 'send_event.php', 3, 0, 0, NULL),
(18, 'main', 'default', 3, 0, 0, 105),
(20, 'modules/test', 'default', 3, 0, 0, NULL),
(21, 'comunica', 'list_messages.php?messages=sent', 3, 0, 0, NULL),
(22, 'browsing', 'main_index.php?op=forum', 3, 0, 0, NULL),
(23, 'comunica', 'list_chatrooms.php', 3, 0, 0, 10),
(24, 'comunica', 'chat.php', 3, 0, 0, NULL),
(25, 'services', 'upload.php', 3, 0, 0, 77),
(26, 'services', 'default', 3, 0, 0, 82),
(27, 'services', 'addnode.php?op=preview', 3, 0, 0, 84),
(28, 'browsing', 'search.php', 3, 0, 0, NULL),
(29, 'browsing', 'external_link.php?file=user_it.html', 3, 0, 0, NULL),
(30, 'browsing', 'default', 3, 1, 0, NULL),
(31, 'browsing', 'edit_user.php', 3, 1, 0, 28),
(32, 'modules/test', 'default', 3, 1, 0, 30),
(33, 'browsing', 'search.php', 3, 1, 0, 28),
(34, 'tutor', 'default', 4, 0, 0, NULL),
(36, 'comunica', 'list_events.php', 4, 0, 0, 16),
(37, 'comunica', 'list_messages.php?messages=sent', 4, 0, 0, 21),
(38, 'comunica', 'send_event.php', 4, 0, 0, 17),
(39, 'comunica', 'send_message.php', 4, 0, 0, 14),
(40, 'comunica', 'list_chatrooms.php', 4, 0, 0, NULL),
(41, 'comunica', 'create_chat.php', 4, 0, 0, NULL),
(57, 'comunica', 'edit_chat.php', 4, 0, 0, NULL),
(42, 'browsing', 'external_link.php?file=practitioner_it.html', 4, 0, 0, 29),
(43, 'tutor', 'edit_tutor.php', 4, 0, 0, 11),
(44, 'main', 'default', 4, 0, 0, 105),
(45, 'browsing', 'view.php', 4, 0, 0, NULL),
(46, 'browsing', 'main_index.php?op=forum', 4, 0, 0, 22),
(47, 'browsing', 'search.php', 4, 0, 0, 28),
(48, 'comunica', 'chat.php', 4, 0, 0, 24),
(49, 'browsing', 'default', 4, 0, 0, 10),
(50, 'services', 'upload.php', 4, 0, 0, 77),
(51, 'services', 'default', 4, 0, 0, 82),
(52, 'services', 'addnode.php?op=preview', 4, 0, 0, 84),
(53, 'tutor', 'tutor.php?op=student&mode=update', 4, 0, 0, NULL),
(54, 'browsing', 'main_index.php?order=struct&hide_visits=0&expand=1', 4, 0, 0, 22),
(55, 'browsing', 'main_index.php?op=forum&order=struct&hide_visits=0', 4, 0, 0, 22),
(56, 'comunica', 'report_chat.php?op=index', 4, 0, 0, NULL),
(58, 'comunica', 'report_chat.php', 4, 0, 0, NULL),
(59, 'tutor', 'tutor.php?op=zoom_student', 4, 0, 0, NULL),
(60, 'tutor', 'tutor_history.php', 4, 0, 0, NULL),
(61, 'tutor', 'tutor_exercise.php', 4, 0, 0, NULL),
(62, 'tutor', 'tutor.php?op=student_notes', 4, 0, 0, NULL),
(63, 'tutor', 'tutor_history_details.php', 4, 0, 0, NULL),
(71, 'browsing', 'external_link.php?file=author_it.html', 1, 0, 0, 29),
(69, 'services', 'author.php', 1, 0, 0, NULL),
(70, 'services', 'default', 1, 0, 0, 105),
(68, 'modules/test', 'default', 4, 0, 0, NULL),
(72, 'main', 'default', 1, 0, 0, 105),
(73, 'browsing', 'main_index.php?op=forum', 1, 0, 0, 22),
(74, 'services', 'addnode.php', 1, 0, 0, 82),
(75, 'services', 'addnode.php?op=preview', 1, 0, 0, 84),
(76, 'services', 'add_exercise.php', 1, 0, 0, 77),
(77, 'abstract', 'indietro', 5, 0, 0, NULL),
(78, 'modules/test', 'default', 1, 0, 0, 77),
(79, 'switcher', 'default', 6, 0, 0, NULL),
(80, 'browsing', 'external_link.php?file=switcher_it.html', 6, 0, 0, 29),
(81, 'main', 'default', 6, 0, 0, 105),
(82, 'abstract', 'annulla', 5, 0, 0, NULL),
(83, 'services', 'edit_node.php?op=edit', 3, 0, 0, 82),
(84, 'abstract', 'edit_note', 5, 0, 0, NULL),
(85, 'services', 'edit_node.php?op=preview', 3, 0, 0, 84),
(86, 'services', 'edit_node.php?op=delete', 3, 0, 0, 77),
(87, 'services', 'edit_node.php?op=edit', 4, 0, 0, 82),
(88, 'services', 'edit_node.php?op=preview', 4, 0, 0, 84),
(89, 'services', 'edit_node.php?op=delete', 4, 0, 0, 77),
(90, 'services', 'edit_node.php?op=edit', 1, 0, 0, 82),
(91, 'services', 'edit_node.php?op=preview', 1, 0, 0, 84),
(92, 'services', 'edit_node.php?op=delete', 1, 0, 0, 77),
(93, 'comunica', 'default', 6, 0, 0, 13),
(94, 'comunica', 'list_chatrooms.php', 6, 0, 0, 40),
(95, 'comunica', 'send_message.php', 6, 0, 0, 14),
(96, 'comunica', 'list_events.php', 6, 0, 0, 16),
(97, 'comunica', 'send_event.php', 6, 0, 0, 17),
(139, 'comunica', 'read_message.php', 4, 0, 0, NULL),
(99, 'comunica', 'list_messages.php?messages=sent', 6, 0, 0, 21),
(100, 'comunica', 'chat.php', 6, 0, 0, 24),
(101, 'comunica', 'create_chat.php', 6, 0, 0, 41),
(102, 'comunica', 'edit_chat.php', 6, 0, 0, 57),
(103, 'comunica', 'delete_chat.php', 6, 0, 0, 57),
(109, 'switcher', 'edit_user.php', 6, 0, 0, 105),
(105, 'abstract', 'home_help_esc', 5, 0, 0, NULL),
(106, 'switcher', 'translation.php', 6, 0, 0, NULL),
(108, 'switcher', 'edit_switcher.php', 6, 0, 0, 105),
(110, 'switcher', 'view_user.php', 6, 0, 0, 105),
(111, 'switcher', 'delete_user.php', 6, 0, 0, 105),
(112, 'switcher', 'add_user.php', 6, 0, 0, 105),
(113, 'switcher', 'add_course.php', 6, 0, 0, 105),
(114, 'switcher', 'edit_course.php', 6, 0, 0, 105),
(115, 'switcher', 'view_course.php', 6, 0, 0, 105),
(116, 'switcher', 'assign_tutor.php', 6, 0, 0, 105),
(117, 'switcher', 'assign_more_tutors.php', 6, 0, 0, 105),
(118, 'switcher', 'subscriptions.php', 6, 0, 0, 105),
(119, 'switcher', 'subscribe.php', 6, 0, 0, 105),
(120, 'switcher', 'edit_instance.php', 6, 0, 0, 105),
(121, 'switcher', 'delete_instance.php', 6, 0, 0, 105),
(122, 'switcher', 'add_instance.php', 6, 0, 0, 105),
(123, 'switcher', 'delete_course.php', 6, 0, 0, 105),
(124, 'abstract', 'home_help_esc_back', 5, 0, 0, NULL),
(125, 'modules/newsletter', 'edit_newsletter.php', 6, 0, 0, 124),
(126, 'modules/newsletter', 'send_newsletter.php', 6, 0, 0, 124),
(127, 'modules/newsletter', 'details_newsletter.php', 6, 0, 0, 124),
(128, 'modules/service-complete', 'edit_completerule.php', 6, 0, 0, 124),
(129, 'modules/service-complete', 'completerule_link_courses.php', 6, 0, 0, 124),
(130, 'admin', 'admin.php', 2, 0, 0, NULL),
(131, 'admin', 'default', 2, 0, 0, 105),
(132, 'admin', 'tester_profile.php', 2, 0, 0, NULL),
(133, 'admin', 'list_users.php', 2, 0, 0, NULL),
(134, 'browsing', 'external_link.php?file=gpl.txt', 5, 0, 0, 29),
(135, 'browsing', 'view.php', 5, 0, 0, NULL),
(136, 'browsing', 'external_link.php?file=guest_it.html', 5, 0, 0, 29),
(140, 'comunica', 'read_message.php', 3, 0, 0, 139),
(138, 'main', 'index.php', 5, 0, 0, NULL),
(141, 'comunica', 'read_message.php', 6, 0, 0, 139),
(142, 'comunica', 'read_event.php', 3, 0, 0, NULL),
(143, 'comunica', 'read_event.php', 4, 0, 0, 142),
(144, 'comunica', 'read_event.php', 6, 0, 0, 142),
(145, 'modules/test', 'edit_answers.php', 1, 0, 0, NULL),
(146, 'browsing', 'exercise.php', 1, 0, 0, NULL),
(147, 'services', 'edit_exercise.php?op=edit', 1, 0, 0, 77),
(148, 'switcher', 'edit_content.php', 6, 0, 0, NULL),
(149, 'browsing', 'view.php', 4, 1, 0, 45),
(150, 'browsing', 'default', 4, 1, 0, 10),
(151, 'browsing', 'search.php', 4, 1, 0, 28),
(152, 'browsing', 'external_link.php?file=gpl.txt', 3, 0, 0, 29),
(153, 'browsing', 'external_link.php?file=gpl.txt', 6, 0, 0, 29),
(154, 'browsing', 'external_link.php?file=gpl.txt', 4, 0, 0, 29),
(155, 'browsing', 'external_link.php?file=gpl.txt', 1, 0, 0, 29),
(156, 'comunica', 'videochat.php', 3, 0, 0, NULL),
(157, 'comunica', 'videochat.php', 4, 0, 0, 156),
(158, 'modules/classroom', 'default', 6, 0, 0, 124),
(159, 'modules/classagenda', 'default', 6, 0, 0, 124),
(160, 'modules/classagenda', 'default', 4, 0, 0, 124),
(161, 'modules/classagenda', 'calendars.php', 6, 0, 0, NULL),
(162, 'modules/classbudget', 'default', 6, 0, 0, NULL),
(163, 'modules/code_man', 'default', 6, 0, 0, 124),
(164, 'modules/classagenda', 'calendars.php', 3, 0, 0, 161),
(165, 'modules/classagenda', 'calendars.php', 4, 0, 0, 161),
(166, 'modules/login', 'default', 6, 0, 0, 124),
(168, 'modules/formmail', 'default', 6, 0, 0, 124),
(169, 'modules/formmail', 'default', 4, 0, 0, 124),
(170, 'modules/badges', 'course-badges.php', 6, 0, 0, NULL),
(171, 'modules/badges', 'user-badges.php', 3, 0, 0, 5);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `menu_page`
--
ALTER TABLE `menu_page`
  ADD PRIMARY KEY (`tree_id`),
  ADD UNIQUE KEY `module` (`module`,`script`,`user_type`,`self_instruction`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `menu_page`
--
ALTER TABLE `menu_page`
  MODIFY `tree_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id of the menu tree for the given module, script, user_type and self instruction', AUTO_INCREMENT=172;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
