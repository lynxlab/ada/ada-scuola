<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<!-- link rel="stylesheet" href="../../../css/main/default/default.css" type="text/css" -->
	<link rel="stylesheet" href="../../../css/main/default/default.css" type="text/css">
</head>
<body>
<a name="top"></a>
<div id="pagecontainer">
<div id="header">
	<template_field class="microtemplate_field" name="header_com">header_com</template_field>
</div> 
<!-- menu -->
    <template_field class="microtemplate_field" name="adamenu">adamenu</template_field>  
<!-- / menu --> 
<!-- PERCORSO -->
<div id="journey" class="ui inverted teal segment">
<i18n>dove sei:</i18n>
    <span>
           <i18n>messaggeria</i18n> > <template_field class="template_field" name="status">status</template_field>
    </span>
</div> 
<!-- / percorso -->

<!-- contenitore -->
<div id="container">

<!--dati utente-->
<div id="user_wrap">
    <div id="status_bar">
        <div class="user_data_default status_bar">
            <template_field class="microtemplate_field" name="user_data_micro">user_data_micro</template_field>
        </div>
    </div>
<!-- / dati utente -->
</div>

<!-- contenuto -->
<div id="content">	 
<div id="contentcontent">


	<div class="first">
		<!-- div class="fontsize-2"><template_field class="template_field" name="status">status</template_field></div-->
	        <form name="form" method="post" accept-charset="UTF-8" action="send_message.php">
	          <div class="edit">
				<div>
					<!-- p -->
	                <!-- i18n class="etichette_form">Modo: </i18n -->
	                <select name="modo" class="select-modo">
	                	<!--  @author  steve@lynxlab.com 14/05/2020 branch ADA SL -->
	                	<option value="M"><i18n>E-mail message</i18n></option>
	                  <option value="S" selected><i18n>ADA message</i18n></option>
							<!--  end mod -->
	            	</select>
	        		<!-- /p -->
					
	        		 <div>
	        		 	<i18n class="etichette_form">Seleziona destinatari </i18n>
	  		  			 <div>
							<p> 
				            	<template_field class="template_field" name="rubrica">rubrica</template_field>

				            </p>
							<div>

	                			<template_field class="template_field" name="student_button">student_button</template_field>
				                <template_field class="template_field" name="tutor_button">tutor_button</template_field>
	                			<template_field class="template_field" name="author_button">author_button</template_field>
				            	<template_field class="template_field" name="admin_button">admin_button</template_field>
	       				     </div>
							<p>
	            				<template_field class="template_field" name="indirizzi">indirizzi</template_field>
				            </p>
						</div>
	         			 <!--  <p>
	            			<input type="submit" name="conferma" value="Conferma">
	        					</p>
	        			  -->		
					 </div>
	                <i18n class="etichette_form">Destinatari: </i18n> 
	                <div id="js_destinatari_sel" name="js_destinatari_sel"><template_field class="template_field" name="destinatari">destinatari</template_field></div>
	            </div>
	            <i18n class="etichette_form_block">Oggetto: </i18n>
				<div>
					<input id="oggetto" type="text" name="titolo" maxlength="255" size="60" value="<template_field class="template_field" name="titolo">titolo</template_field>">
	            </div>
	            <div>
	                <i18n  class="etichette_form_block">Testo: </i18n>
	                <div class="testo_messaggio">
						<textarea name="testo" cols="60" rows="10" WRAP="physical"><template_field class="template_field" name="testo">testo</template_field></textarea>
	            	</div>
	            </div>
				<p>
	                <input type="submit" class="bottone_popup" name="spedisci" value="<i18n>Spedisci</i18n>">
	            	<input type="reset" class="bottone_popup" name="pulisci" value="<i18n>Annulla</i18n>">
				</p>		
	          </div> 
			  <div class="menur">
<!--			  
				<div>
	            	<i18n>Priorita': </i18n>
	                <select name="priorita">
	                    <option value="2" selected>Normale</option>
	                    <option value="1">High</option>
	                	<option value="3">Low</option>
	            	</select>
				</div>
-->
				 
			   
		</div> 
	</form>
	</div> <!-- /first -->		
</div> <!-- contentcontent -->
<div id="bottomcont">
</div>
</div> <!--  / contenuto -->
</div> <!-- / contenitore -->
		<div id="push"></div>
		</div>


<!-- PIEDE -->
<div id="footer">
	<template_field class="microtemplate_field" name="footer">footer</template_field>
</div> <!-- / piede -->   
</body></html>
