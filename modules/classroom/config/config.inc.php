<?php
/**
 * CLASSROOM MODULE.
 *
 * @package			classroom module
 * @author			Giorgio Consorti <g.consorti@lynxlab.com>
 * @copyright		Copyright (c) 2014, Lynx s.r.l.
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU Public License v.2
 * @link			classroom
 * @version			0.1
 */

require_once MODULES_CLASSROOM_PATH.'/include/AMAClassroomDataHandler.inc.php';

define('MODULES_CLASSROOM_EDIT_VENUE',			1); // edit venue action code
define('MODULES_CLASSROOM_EDIT_CLASSROOM',		2); // edit classroom action code

?>
