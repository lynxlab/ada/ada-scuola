# INSTALLAZIONE SEMPLICE CON LO SCRIPT AUTOMATICO
## PASSI

1 Una volta scaricato questo repository, decomprimerlo in una directory visibile dal webserver.

2 Assicurarsi che almeno le cartelle seguenti siano scrivibili dall'utente webserver (es. httpd, Apache2):

* config
* clients
* modules
* layout
* docs

Durante l'uso, dovranno essere scrivibili anche le cartelle:

* upload_files
* log
* services/media


3 Creare tre database e se possibile un utente relativo, con  permessi limitati ai database. Se non viene fatta prima questa operazione, l'installatore proverà a crearli da solo. In questo caso l'utente del DBMS deve avere i permessi di creare un DB

4 Puntare il browser nella directory scelta: partirà l'installatore.
Riempire almeno tutti i campi obbligatori (evidenziati da un asterisco). In particolare, occorre inserire:

- i nomi dei 3 database
- gli utenti e le password relative
- la URL assegnata ad ADA
- una password, che verrà assegnata a tutti gli utenti creati automaticamente (ovvero due coordinatori e un autore)
- tre indirizzi email

Gli indirizzi di email sono usati per le comunicazioni con i docenti e in generale gli utenti che non siano studenti.

* l'indirizzo pubblico della scuola deve essere un indirizzo vero, non viene usato da ADA ma solo esposto;
* l'indirizzo Amministratore Sistema ADA è usato da ADA in alcune comunicazioni di servizio; se si desidera che gli utenti possano rispondere a queste comunicazioni, deve essere un indirizzo vero;
* l'indirizzo noreply viene usato da ADA ma non deve essere necessariamente un indirizzo vero; 
Naturalmente gli indirizzi dovranno essere relativi al dominio in cui è installata ADA. 

Cliccare sul bottone in basso 'Installa ADA'.

5. In caso di problemi, l'installatore si ferma e cerca di informare l'utente sul tipo di problema e su come risolverlo. I problemi principali però sono:

- mancanza dei permessi in scrittura alla directory sopra elencate
- mancanza dei permessi adeguati dell'utente del DB

Se l'installatore NON dovesse avviarsi, ma non è possibile accedere ad ADA, è probabile che ci sia una precedente installazione interrotta. In questo caso, cancellare tutto (compresi i DB) e ricominciare da capo.


## REQUISITI:

* Apache 2.4.6 >
* MySql 5.5.* > oppure MariaDb 10.3.* >
* PHP 7.*> 
* Spazio disco iniziale: circa 100 Mb

